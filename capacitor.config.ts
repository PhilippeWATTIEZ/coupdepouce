import { CapacitorConfig } from '@capacitor/cli';

/** Paramètre de configuration de Capacitor */
const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'cerema',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
