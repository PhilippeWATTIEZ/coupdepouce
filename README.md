# Coup de pouce CEREMA DSI

# DESCRIPTION

_Outil de disponibilité ou non des agents de la DSI du CEREMA, pour intervention sur site ou à distance. Fonctionne sur tout type de terminal._

# INSTALLATION

_Récupérer l'environnement de développement commune à l'équipe projet grace à [l'image docker ionic-coupdepouce](https://hub.docker.com/r/pwcerema69/ionic-coupdepouce)!_

    docker pull pwcerema69/ionic-coupdepouce

_Ouvrir un terminal sur l'image docker, se positionner sur le répertoire /projets_

    cd /projets

_récuperer les sources à jour depuis le repesoritory gitlab de coupdepouce_

    git clone https://gitlab.cerema.fr/PhilippeWATTIEZ/coupdepouce.git

    cd coupdepouce

    npm install

# DEVELOPPER ET TESTER DANS UN NAVIGATEUR

    ionic serve
    ou
    ionic serve --host=0.0.0.0 --port=8080

_Ouvrir http://localhost:8100 ou http://localhost:8080 pour visualiser en live les modifications_

# TESTER LA QUALITE DU CODE

    npm run lint
    npm run report

_Les résultats des la revue de code sont en format html dans le fichier ng-lint-report/report.html._

# EXECUTER LES TESTS UNITAIRES

    npm run test-ci (pour les tests en intégration continue)
    ou
    npm run test (pour les tests en direct avec débuggage depuis le navigateur)

_Ouvrir http://localhost:9876 pour consulter compte-rendu d'exécution des tests unitaires et de non régression_

# CONSTRUIRE UNE NOUVELLE RELEASE

## Browser

    ionic build --prod --release
    ou
    npm run build-ci  _Correspond à l'environnement de production dont les paramétres ORION/ASPRO sont dans environnement.prod.ts_
    ou
    npm run build-rec  _Correspond à l'environnement de recette dont les paramétres ORION-Recette/ASPRO-test sont dans environnement.recette.ts_

_La release est générée sur le répertoire www, pensez à changer le n° de version dans package.json pour modifier les nom des fichiers javascripts_

## Documentation

    npm run compodoc (pour l'intégration continue)
    ou
    npm run docserve (génére la documentation et permet la génération en live)

_La documentation est générée sur le répertoire documentation, il faut ouvrir http://localhost:8080 pour consulter la documentation en live lors de l'édition des sources_
