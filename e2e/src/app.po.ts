import { browser, by, element } from 'protractor';

/**
 * Classe de l'application
 */
export class AppPage {
  /** Fonction pour accèder à la page d'accueil */
  navigateTo() {
    return browser.get('/');
  }

  /** Fonction pour récupérer le titre de la page */
  getPageTitle() {
    return element(by.css('ion-title')).getText();
  }
}
