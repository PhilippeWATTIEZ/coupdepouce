<?php

header("Content-Type: text/plain");
if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$now = date("Y-m-d H:i:s"); 
echo "==================================== alimSITT-empl-alias2.php: $now ==================================================\n";


try {
  // connexion base sqlite
  $dsn = 'sqlite:../db/dsiun.db';
  $db = new PDO($dsn);
  $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  setlocale(LC_TIME, "fr_FR");

  // récupération des agents de la DSI sans matricule
  $nb=0;
  $liste_alias="(''";
  $stmt = $db->prepare("select  substr(distinguishedName,instr(distinguishedName,'(')+1,instr(distinguishedName,')')-instr(distinguishedName,'(')-1) alias from c_agents where not exists ( select email from matricule WHERE email = mail);");
  $stmt->execute();
  while ($value = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $liste_alias.=",'".$value['alias']."'";
    $nb++;
  }
  $liste_alias.=")";
  if ( $verbeux) echo "Matricule à renseigner : $nb, liste= $liste_alias \n";

  if ( $nb > 0) {
    // traitement des matricule absent
    $db->beginTransaction ();
  
    $nbcreer=0;    
    $options= array(
        'soap_version'   => SOAP_1_1,
        'login'          => "WS-USER",
        'password'       => ".XX33UUU",
        'encoding'=>'ISO-8859-1'
    );
    $client = new SoapClient('https://sitt.cerema.fr/t3-services/services/HQTimeServices?wsdl', $options);
    
    $requete="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:hr='http://hr.hq.services.horoquartz.fr/' xmlns:hr1='http://www.horoquartz.fr/t3/service/hr'>
    <soapenv:Header>
        <hr:token>?</hr:token>
    </soapenv:Header>
    <soapenv:Body>
        <hr:findEmployees>
            <!--Optional:-->
            <emplRequest>
                <!--1 or more repetitions:-->
                <hr1:matriList>000000000</hr1:matriList>
                <hr1:start></hr1:start>
                <!--Optional:-->
                <hr1:end></hr1:end>
                <!--Optional:-->
                <hr1:responseShape>
                <!--Zero or more repetitions:-->
                <hr1:data>EMPL.ALIAS</hr1:data>
                <!--Optional:-->
                <hr1:order></hr1:order>
                </hr1:responseShape>
                <!--Optional:-->
                <hr1:criteriaExpression> 1 = 1 OR empl.alias in $liste_alias </hr1:criteriaExpression>
                <!--Optional:-->
                <hr1:language></hr1:language>
            </emplRequest>
        </hr:findEmployees>
    </soapenv:Body>
    </soapenv:Envelope>";
        $xml = new SimpleXMLElement($client->__doRequest($requete,'https://sitt.cerema.fr:443/t3-services/services/HQTimeServices',"",SOAP_1_1,0));
        //
        $xml->registerXPathNamespace('ns2',"http://www.horoquartz.fr/t3/service/hr");
        $xml->registerXPathNamespace('ns3',"http://hr.hq.services.horoquartz.fr/");
        $xml->registerXPathNamespace('ns4',"http://www.horoquartz.fr/ws/core");
        $xml->registerXPathNamespace('ns5',"http://www.horoquartz.fr/ws/sf/SelfServiceWS");
        $xml->registerXPathNamespace('ns6',"http://www.horoquartz.fr/services/hq/qubes/xsd");

        $result = $xml->xpath("//ns2:employees");

        foreach ($result as $node) {
        
            if ( $verbeux) echo "traitement ".$node->xpath("ns2:counters[@cid='EMPL.ALIAS']")[0]->attributes()['val']." : ".$node->xpath('ns4:matri')[0]->__toString()." : ".$node->xpath('ns4:fullName')[0]->__toString()." : ";
            $matri=$node->xpath('ns4:matri')[0]->__toString();
            $alias=$node->xpath("ns2:counters[@cid='EMPL.ALIAS']")[0]->attributes()['val'];
            // insertion de l'enregistrement
            $stmtinsert= $db->prepare("insert into matricule  select '$matri', mail from c_agents where distinguishedName like '%($alias)%';");
            if ( $stmtinsert->execute(array())) {$nbcreer++;if ( $verbeux) echo "création\n";}
            else print_r($stmtinsert);
            unset($stmtinsert);
            }
        echo "Nouveaux matricules: $nbcreer / $nb à insérer\n";
        $db->commit();
        // fin insertion matricules absents avec alias
        
        } 
        else echo "Aucun matricule à renseigner\n";       
        $now = date("Y-m-d H:i:s"); 
        echo "==================================== fin alimSITT-empl-alias2.php: $now ==================================================\n";
    } catch (Exception $e) { 
        echo $e->getMessage();
    }
?>