<?php
//phpinfo();
//exit;
header("Content-Type: text/plain");
if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$now = date("Y-m-d H:i:s"); 
echo "==================================== alimSITT-absence.php: $now ==================================================\n";


try {
    // connexion base sqlite
    $dsn = 'sqlite:../db/dsiun.db';
    $db = new PDO($dsn);
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    setlocale(LC_TIME, "fr_FR");

    // traitement des demandes
    $db->beginTransaction ();
    $stmtinit = $db->prepare("delete from demandes;");
    $stmtinit->execute(array());
    unset($stmtinit);
    $db->commit();

    // récupération des agents de la DSI par leur matricule
    $nb=0;
    $liste_matricule="";
    $stmt = $db->prepare("select matricule from matricule;");
    $stmt->execute();
    while ($value = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $liste_matricule.="<hr1:matriList>".$value['matricule']."</hr1:matriList>";
        $nb++;
    }

    if ( $verbeux) echo "Demmandes à renseigner pour : $nb agents\n";

    if ( $nb > 0) {

        $date = new DateTime();
    
        $date->sub(new DateInterval("P2W"));
        $ddebut=$date->format("Y-m-d");
        $date->add(new DateInterval('P16W'));
        $dfin=$date->format("Y-m-d");

        // traitement des semaines
        $db->beginTransaction ();
        $stmtinit = $db->prepare("delete from demandes");
        $stmtinit->execute(array());
        unset($stmtinit);

        /* $sql="SELECT m.adresse adresse,j.dat dat,j.numsem numsem,j.jour jour,
        CASE WHEN p.absenm = '0' THEN 'C' ELSE 'T' END AS normalmatin,
        CASE WHEN p.absens = '0' THEN 'C' ELSE 'T' END AS normalsoir
        FROM s_etpta.hophjoun j LEFT OUTER JOIN s_etpta.hopmail m ON ( j.matri = m.matri) LEFT OUTER JOIN s_etpta.hopproe p ON (j.profil =p.profil)
        WHERE dat > '$yd-$md-$dd' AND dat < '$yf-$mf-$df'
        AND j.profil <> 'REPO' AND LOWER(m.adresse) IN $liste"; */
        
        $nbcreer=0;
        $options= array(
            'soap_version'   => SOAP_1_1,
            'login'          => "WS-USER",
            'password'       => ".XX33UUU",
            'encoding'=>'ISO-8859-1'
        );
        $client = new SoapClient('https://sitt.cerema.fr/t3-services/services/HQTimeServices?wsdl', $options);
        
        $requete="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:hr='http://hr.hq.services.horoquartz.fr/' xmlns:hr1='http://www.horoquartz.fr/t3/service/hr'>
        <soapenv:Header>
           <hr:token>?</hr:token>
        </soapenv:Header>
        <soapenv:Body>
           <hr:findHjous>
              <!--Optional:-->
              <hjouRequest>
                 <!--1 or more repetitions:-->
                 $liste_matricule
                 <hr1:start>$ddebut</hr1:start>
                 <!--Optional:-->
                 <hr1:end>$dfin</hr1:end>
                 <!--Optional:-->
                 <hr1:responseShape>
                    <!--Zero or more repetitions:-->
                    <hr1:data>HABS.MOTIF</hr1:data>
                    <!--Optional:-->
                    <hr1:order></hr1:order>
                 </hr1:responseShape>
                 <!--Optional:-->
                 <hr1:criteriaExpression></hr1:criteriaExpression>
                 <!--Optional:-->
                 <hr1:language>?</hr1:language>
              </hjouRequest>
           </hr:findHjous>
        </soapenv:Body>
     </soapenv:Envelope>";
     
     $xml = new SimpleXMLElement($client->__doRequest($requete,'https://sitt.cerema.fr:443/t3-services/services/HQTimeServices',"",SOAP_1_1,0));
     //
     $xml->registerXPathNamespace('ns2',"http://www.horoquartz.fr/t3/service/hr");
     $xml->registerXPathNamespace('ns3',"http://hr.hq.services.horoquartz.fr/");
     $xml->registerXPathNamespace('ns4',"http://www.horoquartz.fr/ws/core");
     $xml->registerXPathNamespace('ns5',"http://www.horoquartz.fr/ws/sf/SelfServiceWS");
     $xml->registerXPathNamespace('ns6',"http://www.horoquartz.fr/services/hq/qubes/xsd");

    //$result = $xml->xpath("//ns7:requests[ns7:statusLabel='En attente                                                                                                              ']");
    $result = $xml->xpath("//ns2:absenceList");
    $stmtinsert= $db->prepare("insert into demandes (adresse,dat,motif,valoris,numsem,jour) VALUES (:adresse,:dat,:motif,:valoris,:numsem,:jour);");

    foreach ($result as $node) {
        if ( $node->xpath('ns2:unitCode')[0]->__toString()=='TIME') continue;
        if ( $verbeux) echo $node->xpath('ns2:dat')[0]->__toString()," / ",$node->xpath('ns2:matricule')[0]->__toString()," / ",$node->xpath('ns2:unitCode')[0]->__toString()," / ",  $node->xpath('ns2:reason/ns4:reason')[0]->__toString();
        $date= new DateTime(substr($node->xpath('ns2:dat')[0]->__toString(),0,10));
        $unit=$node->xpath('ns2:unitCode')[0]->__toString();
        if ( $unit == 'DAY') $valoris = 'J';
        else if ( $unit == 'MORNING') $valoris = 'M';
            else $valoris='A';
        $param= array(
            ":adresse"=>$node->xpath('ns2:matricule')[0]->__toString(),
            ":dat"=>$date->format('Y-m-d'),
            ":numsem"=> $date->format('W'),
            ":jour"=>$date->format('w'),
            ":motif"=>$node->xpath('ns2:reason/ns4:reason')[0]->__toString(),
            ":valoris"=>$valoris
        );
        if ( $stmtinsert->execute($param)) {$nbcreer++;if ( $verbeux) echo "création\n";}
            else print_r($stmtinsert);       
        }
        unset($stmtinsert);
        $stmtinit = $db->prepare("update demandes set adresse = ( select email from matricule where matricule = adresse ) where adresse not like '%@cerema.fr'; ");
        $stmtinit->execute(array());
        unset($stmtinit);
        echo "Nouvelles demandes absences: $nbcreer\n";
        $db->commit();
        // fin semaines

        // fin des maj
        if ( $verbeux) echo "Fin du traitement\n";

        $stmt = $db->prepare("update demandes set motif  = (select libcourt from motif where motif.motif = demandes.motif);");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set demandematin = NULL, demandesoir =NULL, provmatin=NULL, provsoir=NULL;");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set demandematin = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','M') );");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set demandesoir = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','A') );");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set provmatin = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','M') ) OR (semaine.dat = e.datfin and e.valfin in ('J','M')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set provsoir = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','A') ) OR (semaine.dat = e.datfin and e.valfin in ('J','A')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
        $stmt->execute();
        $stmt = $db->prepare("update semaine set positionmatin = coalesce(demandematin,provmatin,normalmatin),positionsoir = coalesce(demandesoir,provsoir,normalsoir);");
        $stmt->execute();

        // fin des maj
        $now = date("Y-m-d H:i:s"); 
        echo "==================================== fin alimSITT-absence.php: $now ==================================================\n";

    }    
    } catch (Exception $e) { 
        echo $e->getMessage();
    }
?>