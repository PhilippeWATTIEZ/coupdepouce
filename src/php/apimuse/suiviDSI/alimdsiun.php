<?php
//header("Content-Type: text/plain");
header('Content-type: text/plain; charset=utf-8');
if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$params= json_decode(file_get_contents("parametre.json"),true);
if (count($params) != 5) die("parametres absents");
//   user etpta_readonly login password 'cerema';

try {
    $hostname = $params['hostname'];
    $port = $params['port'];
    $dbname = $params['dbname'];
    $username = $params['username'];
    $pwd = $params['pwd'];
    //Connection à temptation
    $dsnh="pgsql:host=$hostname;port=$port;dbname=$dbname;user=$username;password=$pwd";
    $dbh = new PDO($dsnh);
    // connexion base tampon
    $dsn = 'sqlite:../db/dsiun.db';
    $db = new PDO($dsn);

  # Initiate db connexion
  $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //option('db_conn', $db);
  setlocale(LC_TIME, "fr_FR");

  // récupération des agents de la DSI
  $liste="(''";
  $stmt = $db->prepare("select mail from c_agents;");
  $stmt->execute();
  while ($value = $stmt->fetch(PDO::FETCH_ASSOC)) $liste.=",'".$value['mail']."'";
  $liste.=")";
  if ($verbeux) {print_r($result);echo $liste."\n";}
// fin agents
  $date = new DateTime();
  $dimanche= $date->format("w");

  $date->sub(new DateInterval("P".$dimanche."D"));
  $yd=$date->format("Y")+0;
  $md=$date->format("m")+0;
  $dd=$date->format("d")+0;
  $date->add(new DateInterval('P26W'));
  $yf=$date->format("Y")+0;
  $mf=$date->format("m")+0;
  $df=$date->format("d")+0;

  // traitement des conversions
 
  $db->beginTransaction ();
  $stmt = $dbh->prepare("SELECT motif,famille,libelle,tvaloris,inactif,libcourt from s_etpta.hopmoti where tvaloris like '%J%';");
  $stmt->execute();

$nbinch=0;
$nbcreer=0;
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      //print_r($row);
      // vérification de la non modification de l'enregistrement
      if ( $verbeux) echo "traitement ".$row['motif']." : ".$row['libelle']." : ".$row[':conversion']." : ";
      $stmtinchange = $db->prepare("update motif set libelle = :libelle where motif = :motif");
      $stmtinchange->execute(array(':motif'=>$row['motif'],':libelle'=>$row['libelle']));
      // si aucun enregistrement
      if ( $stmtinchange->rowCount() == 0) {
        // insertion de l'enregistrement
        $stmtinsert= $db->prepare("insert into motif (motif,famille,libelle,tvaloris,inactif,libcourt) VALUES (:motif,:famille,:libelle,:tvaloris,:inactif,'?');");
        //print_r($stmtinsert);
        if ( $stmtinsert->execute($row)) {$nbcreer++;if ( $verbeux) echo "création\n";}
        else print_r($stmtinsert);
        unset($stmtinsert);
      } else {$nbinch++;if ( $verbeux) echo "inchangé\n";}
      unset($stmtinchange);
  }
  echo "Nouveaux motifs: $nbcreer, inchangées: $nbinch\n";
  $db->commit();
 
  // fin traitement des conversions
  
  // traitement des demandes
  $db->beginTransaction ();
  $stmtinit = $db->prepare("delete from demandes;");
  $stmtinit->execute(array());
  unset($stmtinit);

  $sql="SELECT m.adresse adresse, j.dat dat,j.motif motif,j.valoris valoris,j.numsem numsem,j.jour jour 
  FROM s_etpta.hophabs j INNER JOIN s_etpta.hopmail m ON (j.matri = m.matri)   
   WHERE dat > '$yd-$md-$dd' AND dat < '$yf-$mf-$df'
    AND valoris IN ('J','M','A') AND m.adresse IN $liste";
    
  if ( $verbeux) echo "$sql\n";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();
  if ( $verbeux) print_r($stmt);
  $nbcreer=0;
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      if ( $verbeux) echo "traitement ".$row['adresse']." : ".$row[':dat']." : ".$row['motif']." : ";
        // insertion de l'enregistrement
        $stmtinsert= $db->prepare("insert into demandes (adresse,dat,motif,valoris,numsem,jour) VALUES (:adresse,:dat,:motif,:valoris,:numsem,:jour);");
        if ( $stmtinsert->execute($row)) {$nbcreer++;if ( $verbeux) echo "création\n";}
        else print_r($stmtinsert);
        unset($stmtinsert);
  }
  echo "Nouvelles demandes: $nbcreer\n";
  $db->commit();
  // fin demandes


  // traitement des semaines
  $db->beginTransaction ();
  $stmtinit = $db->prepare("delete from semaine");
  $stmtinit->execute(array());
  unset($stmtinit);

  $sql="SELECT m.adresse adresse,j.dat dat,j.numsem numsem,j.jour jour,
  CASE WHEN p.absenm = '0' THEN 'C' ELSE 'T' END AS normalmatin,
  CASE WHEN p.absens = '0' THEN 'C' ELSE 'T' END AS normalsoir
  FROM s_etpta.hophjoun j LEFT OUTER JOIN s_etpta.hopmail m ON ( j.matri = m.matri) LEFT OUTER JOIN s_etpta.hopproe p ON (j.profil =p.profil)
  WHERE dat > '$yd-$md-$dd' AND dat < '$yf-$mf-$df'
  AND j.profil <> 'REPO' AND LOWER(m.adresse) IN $liste";

  if ( $verbeux) echo "$sql\n";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();
  
  $nbcreer=0;
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      // insertion de l'enregistrement
      $stmtinsert= $db->prepare("insert into semaine (adresse,dat,numsem ,jour,normalmatin,normalsoir) VALUES (:adresse,:dat,:numsem ,:jour,:normalmatin,:normalsoir);");
      //print_r($stmtinsert);
      if ( $stmtinsert->execute($row)) {$nbcreer++;if ( $verbeux) echo "création\n";}
      else print_r($stmtinsert);
      unset($stmtinsert);
    }
  echo "Nouvelles semaines: $nbcreer\n";
  $db->commit();
  // fin semaines

  // traitement des encours
  $db->beginTransaction ();
  $stmtinit = $db->prepare("delete from encours");
  $stmtinit->execute(array());
  unset($stmtinit);
  $sql="SELECT adresse ,datdeb,ordre,datfin ,valdeb , valfin ,j.motif motif
  FROM s_etpta.hopabsw j inner join  s_etpta.hopmail m ON (j.MATRI = m.MATRI) INNER JOIN s_etpta.hopmoti l ON (j.MOTIF = l.MOTIF) 
  WHERE datfin >= '$yd-$md-$dd' AND datfin <= '$yf-$mf-$df'
  AND annul = '0' AND visa IN ('2','4') AND ( valdeb IN ('J','M','A')  OR valfin IN ('J','M','A')) 
  AND lower(m.adresse) IN  $liste";
  if ( $verbeux) echo "$sql\n";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();
  
  $nbcreer=0;
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      // insertion de l'enregistrement
      $stmtinsert= $db->prepare("insert into encours (adresse ,datdeb,ordre,datfin ,valdeb , valfin ,motif ) VALUES (:adresse ,:datdeb,:ordre,:datfin ,:valdeb , :valfin ,:motif);");
      //print_r($stmtinsert);
      if ( $stmtinsert->execute($row)) {$nbcreer++;if ( $verbeux) echo "création\n";}
      else print_r($stmtinsert);
      unset($stmtinsert);
    }
    echo "Nouveaux encours: $nbcreer\n";
    $db->commit();

    // fin traitements encours

    // fin des maj
      if ( $verbeux) echo "Fin du traitement\n";

      $stmt = $db->prepare("update demandes set motif  = (select libcourt from motif where motif.motif = demandes.motif);");
      $stmt->execute();
      $stmt = $db->prepare("update encours set motif  = (select libcourt from motif where motif.motif = encours.motif);");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set demandematin = NULL, demandesoir =NULL, provmatin=NULL, provsoir=NULL;");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set demandematin = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','M') );");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set demandesoir = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','A') );");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set provmatin = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','M') ) OR (semaine.dat = e.datfin and e.valfin in ('J','M')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set provsoir = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','A') ) OR (semaine.dat = e.datfin and e.valfin in ('J','A')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
      $stmt->execute();
      $stmt = $db->prepare("update semaine set positionmatin = coalesce(demandematin,provmatin,normalmatin),positionsoir = coalesce(demandesoir,provsoir,normalsoir);");
      $stmt->execute();

      // fin des maj
    
  } catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage() . "\n";
    exit;
  }


// fin du traitement
  unset($dbh);unset($db); 


?>

