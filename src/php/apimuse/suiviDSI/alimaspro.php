<?php
//header("Content-Type: text/plain");
header('Content-type: text/plain; charset=utf-8');
function get_json_aspro($url) {
   $options = array(
        CURLOPT_RETURNTRANSFER => true,         // return web page
        CURLOPT_HEADER         => false,        // don't return headers
        CURLOPT_FOLLOWLOCATION => true,         // follow redirects
        CURLOPT_ENCODING       => "",           // handle all encodings
        CURLOPT_USERAGENT      => "php",     // who am i
        CURLOPT_AUTOREFERER    => true,         // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
        CURLOPT_TIMEOUT        => 120,          // timeout on response
        CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
        CURLOPT_POST            => 1,            // i am sending post data
        CURLOPT_POSTFIELDS     => "client_id=coupdepouce_prod_service&client_secret=bxvh5Hu9D6dohCfGzxnuAJlc0wmMyVZJ&grant_type=client_credentials",    // this are my post vars
        CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
        CURLOPT_SSL_VERIFYPEER => false,        //
        CURLOPT_VERBOSE        => 1                //
    );

    $ch  = curl_init("https://orion.cerema.fr/auth/realms/CeremaApps/protocol/openid-connect/token");
    curl_setopt_array($ch,$options);
    $content = curl_exec($ch);
    curl_close($ch);
	
	$token =  json_decode($content);
	$access_token = $token->access_token;
    $headers = [];
	$headers[] = 'Content-Type:application/json';
    $headers[] = "Authorization: Bearer ".$access_token."";

    $ch  = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    /* set return type json */
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
    /* execute request */
    $results = curl_exec($ch);
    /* close cURL resource */
    curl_close($ch);
	return $results;
}


if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$now = date("Y-m-d H:i:s"); 
echo "==================================== alimaspro.php: $now ==================================================\n";
$agents= json_decode(get_json_aspro("https://aspro.cerema.fr/protected/agents?path=CEREMA%2FSG%2FDSI"),true);
$sites= json_decode(file_get_contents("https://aspro.cerema.fr/sites"),true);
try {
 
    $dsn = 'sqlite:../db/dsiun.db';
    $db = new PDO($dsn);

    # Initiate db connexion
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    setlocale(LC_TIME, "fr_FR");

    // vidage des tables c_agents, c_site et c_hierachie
    $stmtinit = $db->prepare("delete from c_agents");
    $stmtinit->execute(array());
    $stmtinit = $db->prepare("UPDATE `sqlite_sequence` SET `seq` = 0 WHERE `name` = 'c_agents';");
    $stmtinit->execute(array());
    $stmtinit = $db->prepare("delete from c_site");
    $stmtinit->execute(array());
    $stmtinit = $db->prepare("delete from c_hierachie");
    $stmtinit->execute(array());
    unset($stmtinit);

    // traitement des sites
    $db->beginTransaction ();
    $nbcreer=0;
    foreach ($sites as $key =>$row) {

        // insertion de l'enregistrement
        if ( $verbeux) echo "traitement ".$row['codeSite']." : ".$row['commune']."\n";
        $stmtinsert = $db->prepare("insert into c_site (codeSite,nom,adresse,codePostal,commune,siret,latitude,longitude)
        VALUES (:codeSite,:nom,:adresse,:codePostal,:commune,:siret,:latitude,:longitude)");
        $params=array(':codeSite'=>$row['codeSite'],':nom'=>$row['nom'],':adresse'=>$row['adresse'],':codePostal'=>$row['codePostal'],':commune'=>$row['commune'],':siret'=>$row['siret'],':latitude'=>$row['latitude'],':longitude'=>$row['longitude']);
        $stmtinsert->execute($params);
        // si aucun enregistrement
        if ( $stmtinsert->rowCount() == 0) print_r($stmtinsert);
        else $nbcreer++;
        unset($stmtinsert);
    }
    echo "Nb sites : $nbcreer\n";
    $db->commit();
    echo "--------------------------------------------------------------------------------------------\n";
    
    // traitement des agents
    $db->beginTransaction ();
    $nbcreer=0;
    foreach ($agents as $key =>$row) {

        // insertion de l'enregistrement
        if ( $verbeux) echo "traitement ".$row['mail']." : ".substr($row['path'],10)." : ".$row['nomSite']."\n";
        $stmtinsert = $db->prepare("insert into c_agents (identifiant,  nom,  prenom,  sidString,  mail,  sexe,  login,  path, distinguishedName,  ville,  adresse, codePostal, nomSite,  codeSite,  telephone,  mobile,  fonction,  superieurHierarchique,  distanceSuperieurHierarchique,  estAdjointDeAgent,  estAdjointDeEntite,  dateSuppression)
        VALUES (:identifiant, :nom, :prenom, :sidString, :mail, :sexe, :login, :path, :distinguishedName, :ville, :adresse, :codePostal, :nomSite, :codeSite, :telephone, :mobile, :fonction, :superieurHierarchique, :distanceSuperieurHierarchique, :estAdjointDeAgent, :estAdjointDeEntite, :dateSuppression)");
        $params=array(':identifiant'=>$row['identifiant'], ':nom'=>$row['nom'], ':prenom'=>$row['prenom'], ':sidString'=>$row['sidString'], ':mail'=>strtolower($row['mail']), ':sexe'=>$row['sexe'], ':login'=>$row['login'], ':path'=>substr($row['path'],10), ':distinguishedName'=>$row['distinguishedName'], ':ville'=>$row['ville'], ':adresse'=>$row['adresse'], ':codePostal'=>$row['codePostal'], ':nomSite'=>$row['nomSite'], ':codeSite'=>$row['codeSite'], ':telephone'=>$row['telephone'], ':mobile'=>$row['mobile'], ':fonction'=>$row['fonction'], ':superieurHierarchique'=>$row['superieurHierarchique'], ':distanceSuperieurHierarchique'=>$row['distanceSuperieurHierarchique'], ':estAdjointDeAgent'=>$row['estAdjointDeAgent'], ':estAdjointDeEntite'=>$row['estAdjointDeEntite'], ':dateSuppression'=>$row['dateSuppression']);
        $stmtinsert->execute($params);
        // si aucun enregistrement
        if ( $stmtinsert->rowCount() == 0) print_r($stmtinsert);
        else $nbcreer++;
        unset($stmtinsert);
    }
    echo "Nb agents : $nbcreer\n";
    $db->commit();
    echo "--------------------------------------------------------------------------------------------\n";
    // traitement des hierarchie
    $racines=array(1,11,13,17,19,23,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97);
    $colors=array('primary','secondary','danger','success','warning','light','medium','dark');
    $num=0;
    $nbcolor=0;
    $hierarchie=array();
    $tri=array();
    $nbtri=1;
    $entites= json_decode(get_json_aspro("https://aspro.cerema.fr/protected/entites?path=CEREMA%2FSG%2FDSI"),true);
    //foreach( $agents as $key => $ag) echo $ag['mail']." => ".$ag['path']."\n";
    echo 'Nom='.$entites['nom']." chemin=".substr($entites['chemin'],10).' responsable='.$entites['responsable']['nom'].' '.$entites['responsable']['prenom']." filtre=1 color=primary niveau=1\n";
    $hierarchie[]=array('nom'=>$entites['nom'],
    'chemin'=>substr($entites['chemin'],10),
    'guid'=>$entites['guid'],
    'libelle'=>$entites['libelle'],
      'rattachement'=>'',
      'responsable'=>$entites['responsable']['identifiant'],
      'niveau'=>1,
      'prof'=>1,
      'color'=>'primary');
    $tri[$entites['responsable']['identifiant']]=$nbtri++;
    foreach ($entites['agents'] as $cle =>$valeur) if ( ! isset($tri[$valeur['identifiant']])) $tri[$valeur['identifiant']]=$nbtri++;
    sousEntite($entites['sousEntite'],2,'DSI',1);
    $db->beginTransaction ();
    $nbcreer=0;
    foreach ($hierarchie as $key =>$row) {

        // insertion de l'enregistrement
        if ( $verbeux) echo "traitement ".$row['nom']." : ".$row['chemin']." : ".$row['libelle']."\n";
        $stmtinsert = $db->prepare("insert into c_hierachie ( nom,  chemin,  guid,  libelle, rattachement, responsable, niveau, color,prof)
        VALUES ( :nom, :chemin, :guid, :libelle, :rattachement, :responsable, :niveau, :color,:prof)");
        $stmtinsert->execute($row);
        // si aucun enregistrement
        if ( $stmtinsert->rowCount() == 0) print_r($stmtinsert);
        else $nbcreer++;
        unset($stmtinsert);
    }
    echo "Nb hierarchies : $nbcreer\n";
    $db->commit();

    // mise à jour des tri
    $db->beginTransaction ();
    $nbmaj=0;
    foreach ($tri as $key =>$val) {

        // maj de l'enregistrement
        if ( $verbeux) echo "traitement $key : $val\n";
        $stmtupdate = $db->prepare("update c_agents set tri = :tri where identifiant = :id");
        $stmtupdate->execute(array(':id'=>$key,':tri'=>$val));
        // si aucun enregistrement
        if ( $stmtupdate->rowCount() == 0) print_r($stmtupdate);
        else $nbmaj++;
        unset($stmtupdate);
    }
    echo "Nb tri : $nbmaj\n";
    $db->commit();
  } catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage() . "\n";
    exit;
  }


// fin du traitement
unset($db); 
$now = date("Y-m-d H:i:s"); 
echo "==================================== fin alimaspro.php: $now ==================================================\n";

function sousEntite($ent,$niveau,$rat,$filtre) {
    global $num,$racines,$nbcolor,$colors,$hierarchie,$tri,$nbtri;
    foreach ($ent as $key => $val) {
		if ( count($val['agents'])) {
			$num++;
			if ( $niveau == 2) $nbcolor++;
			$hierarchie[]=array('nom'=>$val['nom'],
				'chemin'=>substr($val['chemin'],10),
				'guid'=>$val['guid'],
				'libelle'=>$val['libelle'],
				'rattachement'=>$rat,
				'responsable'=>$val['responsable']['identifiant'],
				'niveau'=>$filtre*$racines[$num],
				'prof'=>$niveau,
				'color'=>$colors[$nbcolor]);
			if ( $val['responsable']['identifiant']!= '' && ! isset($tri[$val['responsable']['identifiant']])) $tri[$val['responsable']['identifiant']]=$nbtri++;
			foreach ($val['agents'] as $cle =>$valeur) if ( ! isset($tri[$valeur['identifiant']])) $tri[$valeur['identifiant']]=$nbtri++;
			echo 'Nom='.$val['nom']." chemin=".substr($val['chemin'],10).' responsable='.$val['responsable']['nom'].' '.$val['responsable']['prenom']." rattachement=$rat color= ".$colors[$nbcolor]." filtre= ".($filtre*$racines[$num])." num=$num niveau=$niveau\n";
			if ( is_array($val['sousEntite']) && count($val['sousEntite'])) sousEntite($val['sousEntite'],$niveau+1,basename($val['chemin']),$filtre*$racines[$num]);
		}
    }
}
?>

