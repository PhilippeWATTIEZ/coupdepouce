<?php
//phpinfo();
//exit;
header("Content-Type: text/plain");
if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$now = date("Y-m-d H:i:s"); 
echo "==================================== alimSITT-encours.php: $now ==================================================\n";


try {
    // connexion base sqlite
    $dsn = 'sqlite:../db/dsiun.db';
    $db = new PDO($dsn);
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    setlocale(LC_TIME, "fr_FR");

    // récupération des agents de la DSI par leur matricule
    $nb=0;
    $stmtinsert= $db->prepare("insert into encours (adresse ,datdeb,ordre,datfin ,valdeb , valfin ,motif ) VALUES (:adresse ,:datdeb,:ordre,:datfin ,:valdeb , :valfin ,:motif);");

    // traitement des semaines
    $db->beginTransaction ();
    $stmtinit = $db->prepare("delete from encours");
    $stmtinit->execute(array());
    unset($stmtinit);

    /*  $sql="SELECT adresse ,datdeb,ordre,datfin ,valdeb , valfin ,j.motif motif
FROM s_etpta.hopabsw j inner join  s_etpta.hopmail m ON (j.MATRI = m.MATRI) INNER JOIN s_etpta.hopmoti l ON (j.MOTIF = l.MOTIF) 
WHERE datfin >= '$yd-$md-$dd' AND datfin <= '$yf-$mf-$df'
AND annul = '0' AND visa IN ('2','4') AND ( valdeb IN ('J','M','A')  OR valfin IN ('J','M','A')) 
AND lower(m.adresse) IN  $liste"; */
    
    $nbcreer=0;
    $options= array(
        'soap_version'   => SOAP_1_1,
        'login'          => "WS-USER",
        'password'       => ".XX33UUU",
        'encoding'=>'ISO-8859-1'
    );
    $client = new SoapClient('https://sitt.cerema.fr/t3-services/services/SelfServices?wsdl', $options);

    


    $sql="select distinct login from c_hierachie h inner join c_agents a on ( h.responsable = a.identifiant );";
    if ( $verbeux) echo "$sql\n";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    
    $nbcreer=0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        if ( $verbeux) echo "Demmandes à valider pour : ".$row['login']." valideurs\n";

        $requete='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf="http://sf.hq.services.horoquartz.fr/" xmlns:xsd="http://www.horoquartz.fr/services/hq/sf/xsd">
            <soapenv:Header>
            <sf:role></sf:role>
            <sf:impersonation>'.$row['login'].'</sf:impersonation>
            <sf:token>?</sf:token>
            </soapenv:Header>
            <soapenv:Body>
            <sf:getRequestToValidate>
                    <!--Optional:-->
                    <wvxRequest>
                        <xsd:startDate></xsd:startDate>
                        <xsd:endDate></xsd:endDate>
                        <xsd:sort></xsd:sort>
                        <xsd:type></xsd:type>
                        <xsd:filter>VISA_PENDING</xsd:filter>
                        <xsd:group></xsd:group>
                        <!--Optional:-->
                        <xsd:restriType></xsd:restriType>
                        <!--Optional:-->
                        <xsd:restriValue></xsd:restriValue>
                    </wvxRequest>
                </sf:getRequestToValidate>
                </soapenv:Body>
            </soapenv:Envelope>';
        $xml = new SimpleXMLElement($client->__doRequest($requete,'https://sitt.cerema.fr:443/t3-services/services/SelfServices',"",SOAP_1_1,0));

        $xml->registerXPathNamespace('ns2',"http://www.horoquartz.fr/ws/sf/SelfServiceWS");
        $xml->registerXPathNamespace('ns3',"http://sf.hq.services.horoquartz.fr/");
        $xml->registerXPathNamespace('ns4',"http://www.horoquartz.fr/ws/core");
        $xml->registerXPathNamespace('ns5',"http://www.horoquartz.fr/t3/service/hr");
        $xml->registerXPathNamespace('ns6',"http://www.horoquartz.fr/services/hq/qubes/xsd");
        $xml->registerXPathNamespace('ns7',"http://www.horoquartz.fr/services/hq/sf/xsd");
        $xml->registerXPathNamespace('ns8',"http://www.horoquartz.fr/services/hq/at/xsd");
        $xml->registerXPathNamespace('ns9',"http://www.horoquartz.fr/services/hq/auth/xsd");
        $xml->registerXPathNamespace('ns10',"http://www.horoquartz.fr/services/hq/cm/xsd");
        $xml->registerXPathNamespace('ns11',"http://www.horoquartz.fr/services/hq/sme/xsd");
        $result = $xml->xpath("//ns7:requests[ns7:statusLabel='En attente                                                                                                              ']");

        foreach ($result as $node) {

            if ( $verbeux) echo $node->xpath('ns7:employee/ns4:matri')[0]->__toString()," / ",$node->xpath('ns7:start')[0]->__toString()," / ",$node->xpath('ns7:end')[0]->__toString()," / ",  $node->xpath('ns7:reason/ns4:reason')[0]->__toString();
    
            $units=$node->xpath('ns7:reasonUnitCodeStart')[0]->__toString();
            if ( $units == 'DAY') $valoris = 'J';
            else if ( $units == 'MORNING') $valoris = 'M';
                else $valoris='A';
            $unite=$node->xpath('ns7:reasonUnitCodeStart')[0]->__toString();
            if ( $unite == 'DAY') $valorie = 'J';
            else if ( $unite == 'MORNING') $valorie = 'M';
                else $valorie='A';
            $params=array(
                ':adresse'=>$node->xpath('ns7:employee/ns4:matri')[0]->__toString() ,
                ':datdeb'=>substr($node->xpath('ns7:start')[0]->__toString(),0,10),
                ':ordre'=>$node->xpath('ns7:ordre')[0]->__toString()+0,
                ':datfin'=> substr($node->xpath('ns7:end')[0]->__toString(),0,10),
                ':valdeb'=> $valoris, 
                ':valfin'=> $valorie,
                ':motif'=>$node->xpath('ns7:reason/ns4:reason')[0]->__toString()
            );
            if ( $stmtinsert->execute($params)) {$nbcreer++;if ( $verbeux) echo " => création\n";}
            else print_r($stmtinsert);
        }
    }
    unset($stmtinsert);
    $stmtinit = $db->prepare("update encours set adresse = ( select email from matricule where matricule = adresse ) where adresse not like '%@cerema.fr'; ");
    $stmtinit->execute(array());
    unset($stmtinit);
    echo "Nouvelles demandes en cours: $nbcreer\n";
    $db->commit();
    // fin semaines

    // fin des maj
    if ( $verbeux) echo "Fin du traitement\n";

    $stmt = $db->prepare("update encours set motif  = (select libcourt from motif where motif.motif = encours.motif);");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set demandematin = NULL, demandesoir =NULL, provmatin=NULL, provsoir=NULL;");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set demandematin = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','M') );");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set demandesoir = (select motif from demandes d where semaine.adresse = d.adresse and semaine.dat = d.dat and d.valoris in ('J','A') );");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set provmatin = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','M') ) OR (semaine.dat = e.datfin and e.valfin in ('J','M')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set provsoir = (select motif from encours e where semaine.adresse = e.adresse and ((semaine.dat = e.datdeb and e.valdeb in ('J','A') ) OR (semaine.dat = e.datfin and e.valfin in ('J','A')) OR (semaine.dat > e.datdeb and semaine.dat < e.datfin)));");
    $stmt->execute();
    $stmt = $db->prepare("update semaine set positionmatin = coalesce(demandematin,provmatin,normalmatin),positionsoir = coalesce(demandesoir,provsoir,normalsoir);");
    $stmt->execute();

    // fin des maj
    $now = date("Y-m-d H:i:s"); 
    echo "==================================== fin alimSITT-encours.php: $now ==================================================\n";
  
    } catch (Exception $e) { 
        echo $e->getMessage();
    }
?>