<?php

header("Content-Type: text/plain");
if (isset($_GET['trace'])) $verbeux=true;
else $verbeux=false;
$now = date("Y-m-d H:i:s"); 
echo "==================================== alimSITT-empl-login.php: $now ==================================================\n";


try {
  // connexion base sqlite
  $dsn = 'sqlite:../db/dsiun.db';
  $db = new PDO($dsn);
  $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  setlocale(LC_TIME, "fr_FR");

  // récupération des agents de la DSI sans matricule
  $nb=0;
  $liste_login="(''";
  $stmt = $db->prepare("select c_agents.mail from c_agents where not exists ( select email from matricule WHERE email = mail);");
  $stmt->execute();
  while ($value = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $liste_login.=",'".str_replace("@cerema.fr","",$value['mail'])."'";
    $nb++;
  }
  $liste_login.=")";
  if ( $verbeux) echo "Matricule à renseigner : $nb, liste= $liste_login \n";

  if ( $nb > 0) {
    // traitement des matricule absent
    $db->beginTransaction ();
    $stmtinsert= $db->prepare("insert into matricule (matricule, email) VALUES (:matri,:email);");
  
    $nbcreer=0;    
    $options= array(
        'soap_version'   => SOAP_1_1,
        'login'          => "WS-USER",
        'password'       => ".XX33UUU",
        'encoding'=>'ISO-8859-1'
    );
    $client = new SoapClient('https://sitt.cerema.fr/t3-services/services/HQTimeServices?wsdl', $options);
    
    $requete="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:hr='http://hr.hq.services.horoquartz.fr/' xmlns:hr1='http://www.horoquartz.fr/t3/service/hr'>
    <soapenv:Header>
        <hr:token>?</hr:token>
    </soapenv:Header>
    <soapenv:Body>
        <hr:findEmployees>
            <!--Optional:-->
            <emplRequest>
                <!--1 or more repetitions:-->
                <hr1:matriList>000000000</hr1:matriList>
                <hr1:start></hr1:start>
                <!--Optional:-->
                <hr1:end></hr1:end>
                <!--Optional:-->
                <hr1:responseShape>
                <!--Zero or more repetitions:-->
                <hr1:data>EMPL.IMPLOGIN</hr1:data>
                <hr1:data>EMPL.NOM</hr1:data>
                <!--Optional:-->
                <hr1:order></hr1:order>
                </hr1:responseShape>
                <!--Optional:-->
                <hr1:criteriaExpression> 1 = 1 OR empl.implogin in $liste_login </hr1:criteriaExpression>
                <!--Optional:-->
                <hr1:language></hr1:language>
            </emplRequest>
        </hr:findEmployees>
    </soapenv:Body>
    </soapenv:Envelope>";
        $xml = new SimpleXMLElement($client->__doRequest($requete,'https://sitt.cerema.fr:443/t3-services/services/HQTimeServices',"",SOAP_1_1,0));
        //
        $xml->registerXPathNamespace('ns2',"http://www.horoquartz.fr/t3/service/hr");
        $xml->registerXPathNamespace('ns3',"http://hr.hq.services.horoquartz.fr/");
        $xml->registerXPathNamespace('ns4',"http://www.horoquartz.fr/ws/core");
        $xml->registerXPathNamespace('ns5',"http://www.horoquartz.fr/ws/sf/SelfServiceWS");
        $xml->registerXPathNamespace('ns6',"http://www.horoquartz.fr/services/hq/qubes/xsd");

        $result = $xml->xpath("//ns2:employees");

        foreach ($result as $node) {
            if ( str_contains($node->xpath("ns2:counters[@cid='EMPL.IMPLOGIN']")[0]->attributes()['val'],strtolower($node->xpath("ns2:counters[@cid='EMPL.NOM']")[0]->attributes()['val']))) 
            {
                if ( $verbeux) echo "traitement ".$node->xpath("ns2:counters[@cid='EMPL.IMPLOGIN']")[0]->attributes()['val']." : ".$node->xpath('ns4:matri')[0]->__toString()." : ".$node->xpath('ns4:fullName')[0]->__toString()." : ";
                $param=array(
                    ":matri"=>$node->xpath('ns4:matri')[0]->__toString(),
                    ":email"=>$node->xpath("ns2:counters[@cid='EMPL.IMPLOGIN']")[0]->attributes()['val']."@cerema.fr"
                );
                // insertion de l'enregistrement
                if ( $stmtinsert->execute($param)) {$nbcreer++;if ( $verbeux) echo "création\n";}
                else print_r($stmtinsert);
            }
        }
        echo "Nouveaux matricules: $nbcreer / $nb à insérer\n";
        $db->commit();
        // fin insertion matricules absents avec login

        
        } 
        else echo "Aucun matricule à renseigner\n";
        $now = date("Y-m-d H:i:s"); 
        echo "==================================== fin alimSITT-empl-login.php: $now ==================================================\n";
       
    } catch (Exception $e) { 
        echo $e->getMessage();
    }
?>