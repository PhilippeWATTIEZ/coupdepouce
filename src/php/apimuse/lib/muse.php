<?php
require_once('db.php');

function liste_noms(){
    return find_objects_by_sql("select adresse as email,nompre as lastname from employes  where adresse is not null order by nompre",array());
}

function liste_sites(){
    $sites=array();
    $liste = find_objects_by_sql("select distinct ville as site from c_agents  where ville is not null and ville != '' order by ville",array());
    foreach( $liste as $key => $val) $sites[]=$val->site;
    return $sites;
}

function liste_serv(){
    $sites=array();
    $liste = find_objects_by_sql("select DISTINCT path from c_agents order by 1",array());
    $sites[]="DSI";
    foreach( $liste as $key => $val) $sites[]=$val->path;
    return $sites;
}

function rech_site($site){
  //$sql="select email from agents where site = :site";
  $sql="select mail as email from c_agents where ville = :site";
  return find_objects_by_sql($sql,array(':site'=>$site));
}

function liste_orga(){
    $sql="select o.email,o.id,o.sigle ,o.designation, o.rattachement,o.agent,o.poste, o.site,o.color,o.nb, coalesce(tri2, tri1) as tri, ifnull(s.positionmatin,'?') as matin,ifnull( s.positionsoir,'?') as soir
    from c_organigramme o left outer join semaine s on ( o.email = s.adresse and s.dat = date('now'))
order by coalesce(tri2, tri1), rattachement, designation desc";
    return find_objects_by_sql($sql,array());
}


function liste_geo(){
    $sql="select a.nom || ' ' || a.prenom agent,a.ville,  a.codeSite, latitude, longitude from c_agents a inner join c_site s using (codeSite)";
    return find_objects_by_sql($sql,array());
}

function liste_carto($service){
    $serv=str_replace("-","/",$service)."%";
    $dateval = new DateTime();
    $week=$dateval->format('W');
    $annee=$dateval->format('Y');
    $jour= $dateval->format('w');
	$jour=$jour==0?7:$jour;
    $heure= $dateval->format('G');
    if ( $heure < 12) $champ = "positionmatin";
    else $champ = "positionsoir";
    $sql="SELECT commune, latitude, longitude,
    SUM(case when s1.$champ is null THEN 0 ELSE 1 END) as site,
	SUM(case when s1.$champ = 'TT' THEN 1 ELSE 0 END) as tele,
	SUM(case when s1.$champ = 'C' THEN 1 ELSE 0 END) as conge,
	SUM(case when s1.$champ = 'T' THEN 1 ELSE 0 END) as bureau
from c_site left outer join c_agents a on ( a.ville = commune and a.path like '$serv') 
    left outer join semaine s1 on ( a.mail = s1.adresse and strftime('%Y',s1.dat) = '$annee' and s1.numsem =$week and s1.jour = $jour)
	group by 1,2,3";
    return find_objects_by_sql($sql,array());
}

function liste_semaine($date,$niveau,$site){
    $dateval = new DateTime($date);
    $week=$dateval->format('W');
    $annee=$dateval->format('Y');
    $sql="select h.libelle as designation,h.niveau ,a.nom || ' ' || a.prenom as agent,a.nom,a.tri, a.mail as email, case when responsable = identifiant Then 'medium' else 'light' End as color, 
    ifnull(s1.positionmatin,'?') as matin1, ifnull(s1.positionsoir,'?') as soir1,   ifnull(s1.provmatin,'Rien') as demmatin1, ifnull(s1.provsoir,'Rien') as demsoir1,
    ifnull(s2.positionmatin,'?') as matin2, ifnull(s2.positionsoir,'?') as soir2,   ifnull(s2.provmatin,'Rien') as demmatin2, ifnull(s2.provsoir,'Rien') as demsoir2,
    ifnull(s3.positionmatin,'?') as matin3, ifnull(s3.positionsoir,'?') as soir3,   ifnull(s3.provmatin,'Rien') as demmatin3, ifnull(s3.provsoir,'Rien') as demsoir3,
    ifnull(s4.positionmatin,'?') as matin4, ifnull(s4.positionsoir,'?') as soir4,   ifnull(s4.provmatin,'Rien') as demmatin4, ifnull(s4.provsoir,'Rien') as demsoir4,
    ifnull(s5.positionmatin,'?') as matin5, ifnull(s5.positionsoir,'?') as soir5,   ifnull(s5.provmatin,'Rien') as demmatin5, ifnull(s5.provsoir,'Rien') as demsoir5
    from c_hierachie h inner join c_agents a on ( a.path = h.chemin ) 
    left outer join semaine s1 on ( a.mail = s1.adresse and strftime('%Y',s1.dat) = '$annee' and s1.numsem =$week and s1.jour = 1)
    left outer join semaine s2 on ( a.mail = s2.adresse and strftime('%Y',s2.dat) = '$annee' and s2.numsem =$week and s2.jour = 2)
    left outer join semaine s3 on ( a.mail = s3.adresse and strftime('%Y',s3.dat) = '$annee' and s3.numsem =$week and s3.jour = 3)
    left outer join semaine s4 on ( a.mail = s4.adresse and strftime('%Y',s4.dat) = '$annee' and s4.numsem =$week and s4.jour = 4)
    left outer join semaine s5 on ( a.mail = s5.adresse and strftime('%Y',s5.dat) = '$annee' and s5.numsem =$week and s5.jour = 5)
    where niveau % $niveau = 0 and a.ville like :site
    order by tri";
    return find_objects_by_sql($sql,array(':site'=>$site));
}

function liste_mois($email){
    $sql="select a.nom || ' ' || a.prenom as agent,a.nom, a.mail as email, s1.dat AS date,ifnull(strftime('%d/%m/',s1.dat)||substr(strftime('%Y',s1.dat),1,2)||'-'||strftime('%d/%m/',s5.dat)||substr(strftime('%Y',s5.dat),1,2),strftime('%d/%m/',s1.dat)||substr(strftime('%Y',s1.dat),1,2)||'-31/12/'||substr(strftime('%Y',s1.dat),1,2)) as semaine,
    ifnull(s1.positionmatin,'?') as matin1, ifnull(s1.positionsoir,'?') as soir1,   ifnull(s1.provmatin,'Rien') as demmatin1, ifnull(s1.provsoir,'Rien') as demsoir1, case when cast(strftime('%m',s1.dat)  as Integer) % 2 = 0 Then 'medium' else 'light' End as color1, 
    ifnull(s2.positionmatin,'?') as matin2, ifnull(s2.positionsoir,'?') as soir2,   ifnull(s2.provmatin,'Rien') as demmatin2, ifnull(s2.provsoir,'Rien') as demsoir2, case when cast(strftime('%m',s2.dat)  as Integer) % 2 = 0 Then 'medium' else 'light' End as color2,
    ifnull(s3.positionmatin,'?') as matin3, ifnull(s3.positionsoir,'?') as soir3,   ifnull(s3.provmatin,'Rien') as demmatin3, ifnull(s3.provsoir,'Rien') as demsoir3, case when cast(strftime('%m',s3.dat)  as Integer) % 2 = 0 Then 'medium' else 'light' End as color3,
    ifnull(s4.positionmatin,'?') as matin4, ifnull(s4.positionsoir,'?') as soir4,   ifnull(s4.provmatin,'Rien') as demmatin4, ifnull(s4.provsoir,'Rien') as demsoir4, case when cast(strftime('%m',s4.dat)  as Integer) % 2 = 0 Then 'medium' else 'light' End as color4,
    ifnull(s5.positionmatin,'?') as matin5, ifnull(s5.positionsoir,'?') as soir5,   ifnull(s5.provmatin,'Rien') as demmatin5, ifnull(s5.provsoir,'Rien') as demsoir5, case when cast(strftime('%m',s5.dat)  as Integer) % 2 = 0 Then 'medium' else 'light' End as color5
    from c_agents a 
    left outer join semaine s1 on ( a.mail = s1.adresse and s1.jour = 1)
    left outer join semaine s2 on (  s1.adresse = s2.adresse and strftime('%Y',s2.dat) = strftime('%Y',s1.dat) and s2.numsem =s1.numsem and s2.jour = 2)
    left outer join semaine s3 on (  s2.adresse = s3.adresse and strftime('%Y',s3.dat) = strftime('%Y',s2.dat) and s3.numsem =s2.numsem and s3.jour = 3)
    left outer join semaine s4 on (  s3.adresse = s4.adresse and strftime('%Y',s4.dat) = strftime('%Y',s3.dat) and s4.numsem =s3.numsem and s4.jour = 4)
    left outer join semaine s5 on (  s4.adresse = s5.adresse and strftime('%Y',s5.dat) = strftime('%Y',s4.dat) and s5.numsem =s4.numsem and s5.jour = 5)
    where a.mail=:email 
    order by s1.dat;";
    return find_objects_by_sql($sql,array(':email'=>$email));
}

function liste_services(){
    //$sql="select sigle,ifnull(rattachement,'') as rattachement,designation,responsable,niveau, color from hierachie where responsable > 0 order by responsable";
    $sql="select nom as sigle,ifnull(rattachement,'') as rattachement,libelle as designation,responsable,niveau, color ,chemin from c_hierachie   order by chemin";
    return find_objects_by_sql($sql,array());
}

function liste_recherche($str) {
    $sql="select mail as email, nom || ' ' || prenom as agent, nom from c_agents where lower(nom || ' ' || prenom) like '%$str%' order by nom limit 6";
    return find_objects_by_sql($sql,array());
}

function  valide_email($val){
    $cle = find_object_by_sql("select identifiant cle from c_agents where LOWER(mail) like '".strtolower($val)."';");
    if ( is_null($cle) ) {
        $subject = 'Vérification propriétaire du mail';
        $message = 'Bonjour<br/><br/>Une demande de connexion pour DSI Céréma a été effectué pour '.$val.' inexistant dans ASPRO pour la DSI!<br>La DSIdu Céréma';
        $encoding = "utf-8";

        // Mail header
        $header = "Content-type: text/html; charset=".$encoding." \r\n";
        $header .= "From: webmestre@les-chignoles.montcarra.fr\r\n";
        $header .= 'Reply-To: wattiez.philippe@neuf.fr' . "\r\n";
        $header .= "MIME-Version: 1.0 \r\n";
        $header .= "Content-Transfer-Encoding: 8bit \r\n";
        $header .= "Date: ".date("r (T)")." \r\n";
                     
        $success = mail('philippe.wattiez@cerema.fr', $subject, $message, $header);
        $objet=(object) array('cle'=>'', 'tests' => array(),'ok' => '', 'mail' => $success);
    } else {
        $tests=rand(1000,9999);
        $subject = utf8_encode('Validation connexion DSI Céréma');
        $message = 'Bonjour<br/><br/>Pour valider votre connexion, saississez le code suivant: '.$tests.'.<br>La DSI du Céréma';
        $encoding = "utf-8";

        // Mail header
        $header = "Content-type: text/html; charset=".$encoding." \r\n";
        $header .= "From: webmestre@les-chignoles.montcarra.fr\r\n";
        $header .= 'Reply-To: wattiez.philippe@cerema.fr' . "\r\n";
        $header .= "MIME-Version: 1.0 \r\n";
        $header .= "Content-Transfer-Encoding: 8bit \r\n";
        $header .= "Date: ".date("r (T)")." \r\n";
                     
        $success = mail($val, $subject, $message, $header);
        $objet=(object) array('cle'=>$cle->cle, 'tests' => $tests, 'mail' => $success);
    }
    return $objet;
}
function search_email($email) {
    $sql="select mail from c_agents where lower(mail) like '%".$email."%' order by nom limit 6";
    $liste = find_objects_by_sql($sql,array());
    $retour=array();
    foreach($liste as $key => $val) $retour[]=$val->mail;
    return $retour;
}


function rech_email($email) {
    $sql="select mail as email, nom || ' ' || prenom as agent, nom from c_agents where lower(email) = :email ";
    return find_object_by_sql($sql,array(':email'=>$email));
}

function select_agent($cle){
    return find_object_by_sql("select a.id, a.identifiant, a.nom || ' ' || a.prenom as agent, a.ville as site,a.telephone, mobile ,h.nom as service, fonction, a.mail as email, a.path as complet,h.libelle as  designation, substr(a.prenom,1,1) || substr(a.nom,1,2) as trigramme  from c_agents a inner join c_hierachie h on (a.path = h.chemin)  where identifiant = :cle",array(':cle'=>$cle));
}

function rech_profil($id)
{
    return find_object_by_sql("select a.nom || ' ' || a.prenom as agent, a.ville as site,a.telephone, mobile ,h.nom as service, fonction, a.mail as email, a.path as complet,h.libelle as  designation from c_agents a inner join c_hierachie h on (a.path = h.chemin)  where id = :id",array(':id'=>$id));
}

function select_user($email){
    return find_object_by_sql("select a.id, a.identifiant, a.nom || ' ' || a.prenom as agent, a.ville as site,a.telephone, mobile ,h.nom as service, fonction, a.mail as email, a.path as complet,h.libelle as  designation, substr(a.prenom,1,1) || substr(a.nom,1,2) as trigramme  from c_agents a inner join c_hierachie h on (a.path = h.chemin)  where LOWER(a.mail) = :email",array(':email'=>strtolower($email)));
}

?>
