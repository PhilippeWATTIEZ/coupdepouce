<?php
    $result="";
    if ( isset($_POST['requete']) )
     {
        try {
            $db = new PDO("sqlite:../db/dsiun.db");
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $requete=explode(";",$_POST['requete']);
            foreach ( $requete as $sql) {
                if ( $sql != "" ) {
                    $stmt = $db->prepare($sql);
                    $stmt->execute(array());
                    $result.="<p><pre>$sql:".$stmt->rowCount()."</pre></p>";
                }
            }
 
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
?>
<!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <style>
        div.row {
            margin-left: 10px;
            margin-right: 10px;
        }
    </style>
    </head>

    <body>

         <nav>
            <div class="nav-wrapper">
              <a href="#" class="brand-logo">&nbsp;&nbsp;MAJ base DSIUN</a>
            </div>
          </nav>


  <div class="row">
    <form class="col s12" action="update.php" method="post">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="requete"  name="requete" class="materialize-textarea"></textarea>
          <label for="requete">Requête</label>
        </div>
      </div>
      <div class="row">
          <button class="btn waves-effect waves-light" type="submit" name="action">Exécuter
            <i class="material-icons right">save</i>
          </button>        
      </div>
    </form>
  </div>
    <div class="row">
        <?php echo $result; ?>
    </div>
        <footer class="page-footer">

          <div class="footer-copyright">
            <div class="container">
            © 2020 DSI AERMC
            <a class="grey-text text-lighten-4 right" href="https://suividsiun.lesagencesdeleau.fr">Application suivi DSIUN</a>
            </div>
          </div>
        </footer>

      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>

  </html>
