<?php 
// Include classes 
include_once('tbs_class.php'); // Load the TinyButStrong template engine 
include_once('tbs_plugin_opentbs.php'); // Load the OpenTBS plugin 

// prevent from a PHP configuration problem when using mktime() and date() 
if (version_compare(PHP_VERSION,'5.1.0')>=0) { 
    if (ini_get('date.timezone')=='') { 
        date_default_timezone_set('UTC'); 
    } 
} 


// Initialize the TBS instance 
$TBS = new clsTinyButStrong; // new instance of TBS 
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin 
$mois=array('','Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');
// ------------------------------ 
// Prepare some data for the demo 
// ------------------------------ 
if ( isset($_GET['serv'])) $serv = $_GET['serv'];
else $serv = 29;
$data=array();
$manquant=array();
$donnees=array();

try {
    $db = new PDO("sqlite:../db/dsiun.db");


    $sql =  "select nom as sigle,chemin as complet, libelle as designation from c_hierachie  where niveau = $serv";
    $stmt = $db->prepare($sql);
    if ($stmt->execute(array()) && $obj = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $sigle = $obj['sigle'];
        $designation = $obj['designation'];
    }
    

    $sql = "select  distinct strftime('%m',DAT) as mois from semaine order by dat";
    foreach($db->query($sql) as $row) {
        $manquant[]=$row['mois'];
        $data[$row['mois']]=array();
    }
//    $sql =  "SELECT date(date,'+1 day') as date, periode, ifnull(valeur,position) position, statut, datemaj from position where email = '$email' order by date , periode desc";
    $sql =  "select a.nom || ' ' || a.prenom as agent, a.fonction as poste,a.ville as site, h.niveau,DAT, ifnull(strftime('%m',DAT),'X') as mois, strftime('%d',DAT) as jour, positionmatin,positionsoir, normalmatin, normalsoir, ifnull(demandematin,normalmatin) as demandematin,ifnull(demandesoir,normalsoir) as demandesoir
    from  c_hierachie h, c_agents a left outer join semaine s on ( a.mail = s.adresse)
    where a.path = h.chemin and h.niveau % $serv = 0 	order by tri";
    foreach($db->query($sql) as $row) 
        {
            if ($row['mois']=="X" && $row['agent']!="X") foreach($manquant as $m) {
                $data[$m][$row['agent']]=array();
                $data[$m][$row['agent']]['agent']=$row['agent'];
                $data[$m][$row['agent']]['site']=$row['site'];
                $data[$m][$row['agent']]['poste']=$row['poste'];
            }
            else {
                if ( !isset( $data[$row['mois']][$row['agent']])) $data[$row['mois']][$row['agent']]=array();
                $data[$row['mois']][$row['agent']]['agent']=$row['agent'];
                $data[$row['mois']][$row['agent']]['site']=$row['site'];
                $data[$row['mois']][$row['agent']]['poste']=$row['poste'];
                $data[$row['mois']][$row['agent']]['positionmatin'.($row['jour']+0)]=$row['positionmatin'];
                $data[$row['mois']][$row['agent']]['positionsoir'.($row['jour']+0)]=$row['positionsoir'];
                $data[$row['mois']][$row['agent']]['demandematin'.($row['jour']+0)]=$row['demandematin'];
                $data[$row['mois']][$row['agent']]['demandesoir'.($row['jour']+0)]=$row['demandesoir'];
                $data[$row['mois']][$row['agent']]['normalmatin'.($row['jour']+0)]=$row['normalmatin'];
                $data[$row['mois']][$row['agent']]['normalsoir'.($row['jour']+0)]=$row['normalsoir'];
            }
        }
    foreach($manquant as $m)
        foreach($data[$m] as $key => $tab) {
            for ( $i=1;$i<32;$i++) if ( !isset($tab['positionmatin'.$i])) {
                $data[$m][$key]['positionmatin'.$i]=' ';
                $data[$m][$key]['positionsoir'.$i]=' ';
                $data[$m][$key]['demandematin'.$i]=' ';
                $data[$m][$key]['demandesoir'.$i]=' ';
                $data[$m][$key]['normalmatin'.$i]=' ';
                $data[$m][$key]['normalsoir'.$i]=' ';
            }
        }
    /*
        header("Content-Type: text/plain");
        for ( $i=5;$i>-1;$i--) {
            echo "--------------------------\n$i : ".$manquant[$i]." : ".$mois[$manquant[$i]+0]."\n";
            $donnees=array();
            foreach($data[$manquant[$i]] as $key => $value) $donnees[]=$value;
            //print_r($donnees); 
            echo 'name="m'.$i.'"'."=>".'name="'.$mois[$manquant[$i]+0].'"'."\n";
        } 
        exit;
    */ 
    
    // ----------------- 
    // Load the template 
    // ----------------- 
    $TBS->LoadTemplate("FeuilleService5.xlsx" ,OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document). 
    for ( $i=5;$i>-1;$i--) {
        $TBS->PlugIn(OPENTBS_SELECT_SHEET, "m$i");
        $donnees=array();
        foreach($data[$manquant[$i]] as $key => $value) $donnees[]=$value;
        $TBS->MergeBlock('a', $donnees);  
    }
    $TBS->PlugIn(OPENTBS_SELECT_FILE, 'xl/workbook.xml');
    for ( $i=5;$i>-1;$i--)$TBS->Source = str_replace('name="m'.$i.'"', 'name="'.$mois[$manquant[$i]+0].'"', $TBS->Source);



    
    // ----------------- 
    // Output the result 
    // ----------------- 
    
    // Output the result as a downloadable file (only streaming, no data saved in the server) 
    $TBS->Show(OPENTBS_DOWNLOAD , "FeuilleService5.xlsx"); // Also merges all [onshow] automatic fields. 

    $dbh = null;
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

?>
