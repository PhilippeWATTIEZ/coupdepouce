<?php 
// Include classes 
include_once('tbs_class.php'); // Load the TinyButStrong template engine 
include_once('tbs_plugin_opentbs.php'); // Load the OpenTBS plugin 

// prevent from a PHP configuration problem when using mktime() and date() 
if (version_compare(PHP_VERSION,'5.1.0')>=0) { 
    if (ini_get('date.timezone')=='') { 
        date_default_timezone_set('UTC'); 
    } 
} 


// Initialize the TBS instance 
$TBS = new clsTinyButStrong; // new instance of TBS 
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin 
$mois=array('','Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');
// ------------------------------ 
// Prepare some data for the demo 
// ------------------------------ 
if ( isset($_GET['email'])) $email = $_GET['email'];
else $email = 'philippe.wattiez@cerema.fr';
$data=array();

try {
    $db = new PDO("sqlite:../db/dsiun.db");

    $sql =  "select a.nom || ' ' || a.prenom as agent, a.ville as site,a.path as complet, h.libelle as designation from c_agents a inner join c_hierachie h on ( a.path = h.chemin) where a.mail =  '$email'";
    $stmt = $db->prepare($sql);
    if ($stmt->execute(Array()) && $obj = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $agent = $obj['agent'];
        $site = $obj['site'];
        $poste = '';
        $sigle = $obj['complet'];
        $designation = $obj['designation'];
        $agent1 = $obj['agent'];
        $site1 = $obj['site'];
        $poste1 = $obj['poste'];
        $sigle1 = $obj['complet'];
        $designation1 = $obj['designation'];
        $agent2 = $obj['agent'];
        $site2 = $obj['site'];
        $poste2 = '';
        $sigle2 = $obj['complet'];
        $designation2 = $obj['designation'];
    }
	if ( !isset($site) ) $site = "";
//    $sql =  "SELECT date(date,'+1 day') as date, periode, ifnull(valeur,position) position, statut, datemaj from position where email = '$email' order by date , periode desc";
    $sql =  "select DAT, strftime('%m',DAT) as mois, strftime('%d',DAT) as jour, positionmatin,positionsoir, normalmatin, normalsoir, ifnull(demandematin,normalmatin) as demandematin,ifnull(demandesoir,normalsoir) as demandesoir
    from semaine
    where adresse = '$email'
    order by DAT";
    foreach($db->query($sql) as $row) 
        {
            if ( !isset( $data[$row['mois']])) $data[$row['mois']]=array();
            $data[$row['mois']]['mois']=$mois[$row['mois']+0];
            $data[$row['mois']]['positionmatin'.($row['jour']+0)]=$row['positionmatin'];
            $data[$row['mois']]['positionsoir'.($row['jour']+0)]=$row['positionsoir'];
            $data[$row['mois']]['demandematin'.($row['jour']+0)]=$row['demandematin'];
            $data[$row['mois']]['demandesoir'.($row['jour']+0)]=$row['demandesoir'];
            $data[$row['mois']]['normalmatin'.($row['jour']+0)]=$row['normalmatin'];
            $data[$row['mois']]['normalsoir'.($row['jour']+0)]=$row['normalsoir'];
        }
    
    foreach($data as $key => $tab) {
        for ( $i=1;$i<32;$i++) if ( !isset($tab['positionmatin'.$i])) {
            $data[$key]['positionmatin'.$i]=' ';
            $data[$key]['positionsoir'.$i]=' ';
            $data[$key]['demandematin'.$i]=' ';
            $data[$key]['demandesoir'.$i]=' ';
            $data[$key]['normalmatin'.$i]=' ';
            $data[$key]['normalsoir'.$i]=' ';
        }
    }

    // ----------------- 
    // Load the template 
    // ----------------- 
    $TBS->LoadTemplate("FeuilleAgent.xlsx" ,OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document). 
    
 
    $TBS->PlugIn(OPENTBS_SELECT_SHEET, "Profil hebdomadaire");
    $TBS->MergeBlock('c2', $data);  

    $TBS->PlugIn(OPENTBS_SELECT_SHEET, "Congés validés");
    $TBS->MergeBlock('b1', $data);  

    // -------------------------------------------- 
    // Merging and other operations on the template 
    // -------------------------------------------- 
    $TBS->PlugIn(OPENTBS_SELECT_SHEET, "Congés demandés");
    // Merge data in the Workbook (all sheets) 
    $TBS->MergeBlock('a', $data);  

    
    // ----------------- 
    // Output the result 
    // ----------------- 
    
    // Output the result as a downloadable file (only streaming, no data saved in the server) 
    $TBS->Show(OPENTBS_DOWNLOAD , "FeuilleAgent.xlsx"); // Also merges all [onshow] automatic fields. 

    $dbh = null;
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

?>
