<?php
# ############################################################################ #
#                                                                              #
#  # LIMONADE BLOG EXAMPLE #                                                   #
#                                                                              #
#  ---                                                                         #
#                                                                              #
#  by [Fabrice Luraine](http://github.com/sofadesign/) and                     #
#  [Mathias Standaert](http://github.com/organicweb/)                          #
#                                                                              #
#  This basic blog application will show you how to use                        #
#  the [Limonade PHP](http://www.sofa-design.net/limonade) micro-framework     #
#                                                                              #
#  This code is for learning purpose and is not intended to be used "as is"    #
#  in a production environement.                                               #
#                                                                              #
#  ---                                                                         #
#                                                                              #
#  Copyright (c) 2009 Fabrice Luraine, Mathias Standaert                       #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person                 #
#  obtaining a copy of this software and associated documentation              #
#  files (the "Software"), to deal in the Software without                     #
#  restriction, including without limitation the rights to use,                #
#  copy, modify, merge, publish, distribute, sublicense, and/or sell           #
#  copies of the Software, and to permit persons to whom the                   #
#  Software is furnished to do so, subject to the following                    #
#  conditions:                                                                 #
#                                                                              #
#  The above copyright notice and this permission notice shall be              #
#  included in all copies or substantial portions of the Software.             #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,             #
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES             #
#  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                    #
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                 #
#  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                #
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR               #
#  OTHER DEALINGS IN THE SOFTWARE.                                             #
#                                                                              #
# ############################################################################ #
# 0. Loading limonade framework
require_once('lib/limonade.php');
//require_once('pdo_sqlite.php');
require_once('lib/muse.php');

# 1. Setting global options of our application
function configure()
{

  # A. Initiate db connexion
	
  # Initiate db connexion
	$dsn = 'sqlite:db/dsiun.db';
	try
	{
	  $db = new PDO($dsn);
	}
	catch(PDOException $e)
	{
	  halt("Connexion failed: ".$e); # raises an error / renders the error page and exit.
	}
	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
  option('db_conn', $db);
  date_default_timezone_set('Europe/Paris'); 
	
	# Other options
	setlocale(LC_TIME, "fr_FR");
}

# 2. Setting code that will be executed bfore each controller function
function before()
{
 header("Access-Control-Allow-Origin: *");
 header("Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT");
 header('Access-Control-Allow-Headers: X-Requested-With');
 header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');
 header('Access-Control-Expose-Headers: Content-Length,Content-Range');
}

# 3. Defining routes and controllers
# ----------------------------------------------------------------------------
# RESTFul map:
#
#  HTTP Method |  Url path         |  Controller function
# -------------+-------------------+-------------------------------------------
#   GET        |  /evenements/:cle |  evenements_index
#   GET        |  /membres         |  membres_index
#   GET        |  /emails/:fil     |  emails_filtre
#   GET        |  /email/:val      |  email_val
#   GET        |  /membre/:cle     |  membres_show
#   GET        |  /evenement/:id/:cle   |  evenements_show 
#   GET        |  /present/:id/:cle   |  evenements_present 
#   GET        |  /absent/:id/:cle   |  evenements_absent 
#   GET        |  /evenements/new  |  evenements_new 
#   POST       |  /posts           |  evenements_create
# -------------+-------------------+-------------------------------------------
#
# matches GET /
dispatch('/', 'orga');
  function orga()
  {
    return json(true); # redirects to the index
  }

# matches GET /orga
dispatch('/orga', 'orga_liste');
  function orga_liste()
  {
    $orga = liste_orga();
    return json($orga); 
  }

# matches GET /geo
dispatch('/geo', 'geo_liste');
function geo_liste()
{
  $geo = liste_geo();
  return json($geo); 
}

# matches GET /carto/serv
dispatch('/carto/:serv', 'carto_liste');
function carto_liste()
{
  $carto = liste_carto(params('serv'));
  return json($carto); 
}


# matches GET /serv
dispatch('/serv', 'serv_liste');
function serv_liste()
{
  $services = liste_serv();
  return json($services); 
}

# matches GET /services
dispatch('/services', 'services_liste');
function services_liste()
{
  $services = liste_services();
  return json($services); 
}

# matches GET /sites
dispatch('/sites', 'sites_liste');
function sites_liste()
{
  $sites = liste_sites();
  return json($sites); 
}

# matches GET /semaine/:date/:niveau/:site
dispatch('/semaine/:date/:niveau/:site', 'semaine_liste');
function semaine_liste()
{
  $semaine = liste_semaine(params('date'),params('niveau'),params('site'));
  return json($semaine); 
}

# matches GET /mois/:email
dispatch('/mois/:email', 'mois_liste');
function mois_liste()
{
  $mois = liste_mois(params('email'));
  return json($mois); 
}

# matches GET /email/:email
dispatch('/email/:email', 'email_rech');
function email_rech()
{
  $email = rech_email(params('email'));
  return json($email); 
}

# matches GET /site/:site
dispatch('/site/:site', 'site_rech');
function site_rech()
{
  $email = rech_site(params('site'));
  return json($email); 
}

# matches GET /profil/:id
dispatch('/profil/:id', 'profil_rech');
function profil_rech()
{
  $profil = rech_profil(params('id'));
  return json($profil); 
}

# matches GET /recherche/:str
dispatch('/recherche/:str', 'recherche_liste');
function recherche_liste()
{
  $recherche = liste_recherche(params('str'));
  return json($recherche); 
}

# matches GET /search/:email
dispatch('/search/:email', 'email_search');
function email_search()
{
  $email = search_email(strtolower(params('email')));
  return json($email); 
}

# matches GET /valide/:email
dispatch('/valide/:email', 'email_valide');
function email_valide()
{
  $email = valide_email(strtolower(params('email')));
  return json($email); 
}


# matches GET /agent/:cle
dispatch('/agent/:cle', 'agent_select');
  function agent_select()
  {
    $agent = select_agent(params('cle'));
    return json($agent); 
  }

# matches GET /user/:email
dispatch('/user/:email', 'user_select');
function user_select()
{
  $user = select_user(params('email'));
  return json($user); 
}

# 4. Running the limonade blog app
run();
?>
