import { Component, OnInit, Input } from '@angular/core';

/**
 * Composant contenu de page
 */
@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
/**
 * Classe associée au composant contenu de page
 */
export class ExploreContainerComponent implements OnInit {
  /** Nom de l"accplication */
  @Input() name: string;

  /**
   * Constructeur de la classe
   */
  constructor() { }

  /**
   * Fonction d'initialisation du contenu de la page
   */
  ngOnInit() {}

}
