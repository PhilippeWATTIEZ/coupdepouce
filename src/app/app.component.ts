import { Component } from '@angular/core';

/**
 * Composant de l'application
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
/**
 * Classe associée au composant de l'application
 */
export class AppComponent {
  /**
   * Constructeur de la classe associée au composant de l'application
   */
  constructor() {}
}
