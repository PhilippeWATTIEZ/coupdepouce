import { TestBed } from '@angular/core/testing';
import { AgentService } from './agent.service';
import { OAuthService, UrlHelperService, OAuthLogger,OAuthModule,DateTimeProvider } from 'angular-oauth2-oidc';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentService,OAuthService, UrlHelperService,DateTimeProvider,OAuthLogger],
      imports: [OAuthModule.forRoot(),HttpClientTestingModule]
    });
    service = TestBed.inject(AuthenticationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
