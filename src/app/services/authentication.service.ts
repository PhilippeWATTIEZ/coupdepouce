import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AgentService } from './agent.service';
import { OAuthService } from 'angular-oauth2-oidc';

/**
 * Injection
 */
@Injectable({
  providedIn: 'root'
})
/**
 * Classe de service permettant de gérer l'authentification des utilisateur
 */
export class AuthenticationService  {
  /** Objet observable reflétant l'état de connexion ou pas de l'utilisateur */
  authState = new BehaviorSubject(false);
  /**
   * Constructeur du service
   *
   * @param agent : service gérant les information de l'agent connecté
   * @param oauthService : service de connexion OpenID connect
   */
  constructor(
    private agent: AgentService,
    private oauthService: OAuthService,
  ) {}

  /**
   * Fonction de connection à Orion
   */
  login() {
    this.oauthService.initCodeFlow();
  }

  /**
   * Fonctionde déconnection à ORION
   */
  logout() {
    this.agent.videAgent();
    this.oauthService.logOut();
    this.authState.next(false);
  }

  /**
   * Fonction retournant l'état actuel de l'authentification de l'utilisateur
   *
   * @returns boolean : true si connecté ou false sinon
   */
  isAuthenticated() {
    return (this.agent.getAgent() != null && this.authState.value);
  }
}
