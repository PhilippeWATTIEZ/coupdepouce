import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiserviceService } from './apiservice.service';
import { OAuthService } from 'angular-oauth2-oidc';


describe('ApiserviceService', () => {
  let service: ApiserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiserviceService,OAuthService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ApiserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
