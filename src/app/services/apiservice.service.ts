import { Injectable, } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Agents } from '../models/agents';
import { Services } from '../models/services';
import { Semaine } from '../models/semaine';
import { Mois } from '../models/mois';
import { Emails } from '../models/emails';
import { Sites } from '../models/sites';
import { apiurl } from '../../environments/environment';

/**
 * Injection
 */
@Injectable({
  providedIn: 'root'
})

/**
 * Classe des services des appels API REST pour alimentation des classes d'objet
 */
export class ApiserviceService {

  /**
   * Constructeur de la classe de service avec le Client HTTP nécessaire au requete HTTP API REST
   *
   * @param http
   */
  constructor(private http: HttpClient) { }

  /**
   * Fonction recherchant en backoffice l'agent correspondant à la clé en paramétre
   *
   * @param cle Identifiant de l'agent à retourner
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
  getAgent(cle: string): Observable<any> {
    return this.http.get(`${apiurl}agent/${cle}`);
    }

  /**
   * Fonction recherchant en backoffice l'agent correspondant à l'email en paramétre
   *
   * @param email eamil de l'agent à retourner
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
     getUser(email: string): Observable<any> {
      return this.http.get(`${apiurl}user/${email}`);
      }

  /**
   * Fonction recherchant en backoffice le nom et prénom de l'agent correspondant à l'email en paramétre
   *
   * @param email Email de l'agent à retourner
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    getEmail(email: string): Observable<any> {
    return this.http.get(`${apiurl}email/${email}`);
    }

  /**
   * Fonction recherchant en backoffice un code de vérification envoyé par mail sur la boite de l'utilisateur
   *
   * @param email Email de l'agent à vérifier
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    validEmail(email: string): Observable<any> {
    return this.http.get(`${apiurl}valide/${email}`);
    }

  /**
   * Fonction recherchant en backoffice les email correspondant au masque de recherche en paramétre
   *
   * @param email Masque de recherche
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    searchEmails(email: string): Observable<any> {
    return this.http.get(`${apiurl}search/${email}`);
    }

  /**
   * Fonction recherchant en backoffice les informations de l'agent correspondant à l'ID en paramétre
   *
   * @param id Identifiant de l'agent à retourner
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    getProfil(id: string): Observable<any> {
    return this.http.get(`${apiurl}profil/${id}`);
    }

  /**
   * Fonction recherchant en backoffice l'email correspondant au nom et prénom de l'agent en paramétre
   *
   * @param val nom ou prénom de l'agent à chercher
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    rechercheEmails(val: string): Observable<Emails[]> {
    return this.http.get(`${apiurl}recherche/${val}`).pipe(
      map(
        (jsonArray: any[]) => jsonArray.map(jsonItem => Emails.fromJson(jsonItem))
      )
    );
  }

  /**
   * Fonction recherchant en backoffice les noms des sites du Cerema
   *
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
  getSites(): Observable<any> {
    return this.http.get(`${apiurl}sites`);
  }

  /**
   * Fonction recherchant en backoffice les noms des services de la DSI du Cerema
   *
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
  getServ(): Observable<any> {
    return this.http.get(`${apiurl}serv`);
  }

  /**
   * Fonction recherchant en backoffice les sites du Cerema en calculant les nombres (absolu/présents/télétravailleurs/absents)
   *
   * @param service nom du service
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
  getCarto(service: string): Observable<Sites[]> {
    return this.http.get(`${apiurl}carto/${service}`).pipe(
      map(
        (jsonArray: any[]) => jsonArray.map(jsonItem => Sites.fromJson(jsonItem))
      )
    );
  }

  /**
   * Fonction recherchant en backoffice les agents de l'organigramme de la DSI du Cerema
   *
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    getAgents(): Observable<Agents[]> {
    return this.http.get(`${apiurl}orga`).pipe(
      map(
        (jsonArray: any[]) => jsonArray.map(jsonItem => Agents.fromJson(jsonItem))
      )
    );
  }

  /**
   * Fonction recherchant en backoffice les services de la DSI du Cerema
   *
   * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
   */
    getServices(): Observable<Services[]> {
      return this.http.get(`${apiurl}services`).pipe(
        map(
          (jsonArray: any[]) => jsonArray.map(jsonItem => Services.fromJson(jsonItem))
        )
      );
    }

    /**
     * Recherche des positions des agents d'un site et/ou service pour une date donnée
     *
     * @param date Date fixant la semaine à traiter
     * @param niveau Identifiant du service pour filtrer les agents concernés
     * @param site Identifiant du site pour filtrer les agents
     * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
     */
    getSemaine(date: string,niveau: number,site: string): Observable<Semaine[]> {
      return this.http.get(`${apiurl}semaine/${date}/${niveau}/${site}`).pipe(
        map(
          (jsonArray: any[]) => jsonArray.map(jsonItem => Semaine.fromJson(jsonItem))
        )
      );
    }

    /**
     * Recherche les positions d'un agent pour un mois donné
     *
     * @param email Identifiant de l'agent à traiter
     * @returns Un observable auquel il faut s'abonner pour récupérer les changements d'état
     */
    getMois(email: string): Observable<Mois[]> {
      return this.http.get(`${apiurl}mois/${email}`).pipe(
        map(
          (jsonArray: any[]) => jsonArray.map(jsonItem => Mois.fromJson(jsonItem))
        )
      );
    }
}
