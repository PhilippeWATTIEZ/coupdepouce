import { Injectable } from '@angular/core';
import { Agent } from '../models/agent';
import { Storage } from '@capacitor/storage';
import { BehaviorSubject } from 'rxjs';

/**
 * Injection
 */
@Injectable({
  providedIn: 'root'
})

/**
 * Classe contenant les services de stockage locale des données d'identification
 */
export class AgentService{
  /** Observable sur les informations Agents pour les abonnements des composants qui sont impactés par tout changement */
  private agentSubject: BehaviorSubject<Agent> = new BehaviorSubject(null);
  /**
   * Contructeur de la classe
   */
   constructor() {
    this.getStorage();
  }

  /**
   * Fonction qui récupère les informations locales et les stocke dans la propriété agent
   *
   * @returns Aucune valeur
   */
  async getStorage(): Promise<void> {

      const ret = await Storage.get({ key: 'agent' });
      console.log('Valeur de retour de Storage.get()', JSON.parse(ret.value));
      if ( ret != null ) {
        this.agentSubject.next(JSON.parse(ret.value));
      }
  }

  /**
   * Fonction qui supprime le stockage local
   *
   * @returns aucune valeur
   */
  videStorage(): Promise<void>{
    this.agentSubject.next(null);
    return Storage.remove({ key: 'agent' });
  }

  /**
   * Fonction qui  stoke en locale les information contenu dans la propriété agent
   *
   * @returns aucune valeur
   */
  async setStorage(agent: Agent): Promise<void>{
    await Storage.set({key:'agent', value: JSON.stringify(agent)}).then(() => console.log('Storage agent: '+agent.complet));
  }

  /**
   * Fonction retournant la valeur de l'attribut identifiant de l'agent
   *
   * @returns Valeur de l'identifiant de l'agent
   */
  getCle(): string{
    return this.agentSubject.value.identifiant;
  }

  /**
   * Fonction qui retourne l'agent stocké en local sur le terminal utilisateur
   *
   * @returns Objet Agent de l'utilisateur
   */
  getAgent(): Agent {
    return this.agentSubject.value;
  }

  /**
   * Function permettant de modifier la propriété actuelle agent, la stocker en locale
   * et de prévenir les abonnés à cette propriété d'un changement
   *
   * @param agent Nouvelle valeur à stocker en locale (suite connexion)
   * @returns aucune valeur
   */
  setAgent(agent: Agent): void{
    this.setStorage(agent);
    this.agentSubject.next(agent);
  }

  /**
   * Fonction appeler lors de la déconnexion pour vider les informations locales relative à l'utilisateur
   *
   * @returns aucune valeur
   */
  videAgent(): void{
    this.videStorage();
  }


  /**
   * Fonction qui retourne le service d'abonnement à l'information concernant l'utilisateur connecté
   *
   * @returns L'observable auquel il faut s'abonner pour connaitre tous les changement de valeur lui arrivant
   */
  getObservable(): BehaviorSubject<Agent> {
      return this.agentSubject;
  }
}
