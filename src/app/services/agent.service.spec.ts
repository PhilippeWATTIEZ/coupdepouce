import { TestBed, waitForAsync } from '@angular/core/testing';
import { AgentService } from './agent.service';
import { Agent } from '../models/agent';


describe('AgentService', () => {
  let service: AgentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgentService);
  });


  afterEach(() => {
    service.videStorage().then(() => console.log('Locale storage vidé'));
    service = null;
  });

  it('should be created',() => {
    expect(service).toBeTruthy();
  });

  it('should be empty the first time is it called', () => {
    expect(service.getAgent()).toBeNull();
  });

  /*
  it('should be equal the next time is it called', () => {
    const agent: Agent = {id:'90',identifiant:'a2e8ca99-890c-47a0-aca0-5c8e3d0477b1',
      agent:'WATTIEZ Philippe',site:null,telephone:'+33 482917538',mobile:'+33 661649314',
      service:'EPTN',fonction:'Chef de pôle',email:'philippe.wattiez@cerema.fr',
      complet:'DSI/SGI/EPTN',designation:'Pôle études, projets et transformation numérique',
      trigramme:'PWA'};
    service.setAgent(agent);
    service.getObservable().subscribe(x => expect(x).toEqual(agent));
  });
*/

});
