import { Component } from '@angular/core';

/**
 * Composant associé au tabs de bas de page contenant les icones des onglets
 */
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
/**
 * Classe associé au tabs de bas de page contenant les icones des onglets
 */
export class TabsPage {

  /**
   * Constructeur de la classe associé au tabs de bas de page contenant les icones des onglets
   */
  constructor() {}

}
