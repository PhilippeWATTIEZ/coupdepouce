import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Tabs4Page } from './tabs4.page';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { NavController } from '@ionic/angular';
import { OAuthService, UrlHelperService, OAuthLogger,OAuthModule,DateTimeProvider } from 'angular-oauth2-oidc';
import { AuthenticationService } from 'src/app/services/authentication.service';

TestBed.configureTestingModule({
  imports: [
          HttpClientTestingModule,
  ]
});
describe('Tabs4Page', () => {
  let component: Tabs4Page;
  let fixture: ComponentFixture<Tabs4Page>;
  const agent: Agent = {
    id: '90',
    identifiant: 'a2e8ca99-890c-47a0-aca0-5c8e3d0477b1',
    agent: 'WATTIEZ Philippe',
    site: null,
    telephone: '+33 482917538',
    mobile: '+33 661649314',
    service: 'EPTN',
    fonction: 'Chef de pôle',
    email: 'philippe.wattiez@cerema.fr',
    complet: 'DSI/SGI/EPTN',
    designation: 'Pôle études, projets et transformation numérique',
    trigramme: 'PWA'
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tabs4Page],
      providers: [ApiserviceService,NavController,AuthenticationService,AgentService,OAuthService,
        UrlHelperService, OAuthLogger,DateTimeProvider],
      imports: [IonicModule.forRoot(),HttpClientTestingModule,RouterTestingModule,OAuthModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Tabs4Page);
    component = fixture.componentInstance;
    component.api = TestBed.inject(ApiserviceService);
    component.nav = TestBed.inject(NavController);
    component.user  = TestBed.inject(AgentService);
    component.user.setAgent(agent);
    fixture.detectChanges();
  }));

  afterEach(() => {
    component.user.videStorage().then(() => console.log('Locale storage vidé'));
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
