import { Component, OnInit, OnDestroy } from '@angular/core';
import { Agent } from '../models/agent';
import { NavController } from '@ionic/angular';
import { AgentService } from '../services/agent.service';
import * as Leaflet from 'leaflet';
import { ApiserviceService }  from '../services/apiservice.service';
import { Services } from '../models/services';
import { AuthenticationService } from '../services/authentication.service';

/**
 * Composant associé à la page Onglet 4: carte des sites
 */
@Component({
  selector: 'app-tabs4',
  templateUrl: './tabs4.page.html',
  styleUrls: ['./tabs4.page.scss'],
})

/**
 * Classe associé à la page onglet 4: carte des sites
 */
export class Tabs4Page implements OnInit, OnDestroy {
  /** Agent correspondant à l'utilisateur connecté */
  public agent: Agent;
  /** Objet Map: Leaflet carte */
  public map: Leaflet.Map;
  /** Variable centre de la carte */
  public center = [
    46.5593567,2.364429,
  ];
  /** Tableau des marqueurs de carte des sites du Cerema */
  markers: Leaflet.Marker[]=[];
  /** Tableau des services pour liste déroulantes */
  services: Services[] = [];
  /** Variable contenant la valeur actuelle du service sélectionné */
  service: number = 0;
  /** Variable associé à la liste déroulante des services */
  selectedService: number = 1;
  /** Options pour la liste déroulantes des services */
  customPopoverOptions: any = {
    header: 'Services de la DSI',
    subHeader: 'Choix du service',
    cssClass: 'grandelargeur'
  };
  /** Icone pour les sites n'ayant que des agents absents */
  absentIcon: Leaflet.Icon = Leaflet.icon({
    iconUrl: 'assets/icon/absent.png',
    iconSize: [30,30],
    popupAnchor: [0, -10]
  });;
  /** Icone pour les sites n'ayant aucun agent de la DSI */
  aucunIcon: Leaflet.Icon = Leaflet.icon({
    iconUrl: 'assets/icon/aucun.png',
    iconSize: [30,30],
    popupAnchor: [0, -10]
  });;
  /** Icone pour les sites ayant au moins un agent sur place */
  presentIcon: Leaflet.Icon = Leaflet.icon({
    iconUrl: 'assets/icon/present.png',
    iconSize: [30,30],
    popupAnchor: [0, -10]
  });;
  /** Icone pour les sites n'ayant au moins un agent en télétravail */
  teleIcon: Leaflet.Icon = Leaflet.icon({
    iconUrl: 'assets/icon/tele.png',
    iconSize: [30,30],
    popupAnchor: [0, -10]
  });;
  /** Options pour la carte Leaflet */
  public options = {
    zoom: 6,
    maxZoom: 18,
    zoomControl: false,
    preferCanvas: true,
    attributionControl: true,
    center: this.center,
    layers: [
      new Leaflet.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
    ],
  };

  /**
   * Constructeur avec les services nécessaires pour le fonctionnement de la page
   *
   * @param api Service d'accès aux informations en backoffice via API REST
   * @param user Service de stockage en locale des informations de connexion
   * @param nav Service de controle de la navigation
   */
  constructor(public api: ApiserviceService,public user: AgentService,public nav: NavController, private auth: AuthenticationService) {
    this.user.getObservable().subscribe(x => {
      this.agent = x;
      if ( x !== null && x !== undefined && x.email !== '') {
        this.api.getServices().subscribe(y => {
          this.services = y;
          this.reinit();
        });
      }
    });
  }

  /**
   * Fonction implémentée avec la classe pour initialiser les éléments de la page dans le cycle de vie
   *
   *
   * - vérification que l'utilisateur est connecté, sinon affichage de la page connexion
   * - alimentation de la liste déroulante des services
   */
  ngOnInit() {
    this.agent=this.user.getAgent();
      if (  this.agent !== null &&  this.agent !== undefined &&  this.agent.email !== '') {
        this.api.getServices().subscribe(y => {
          this.services = y;
          this.reinit();
        });
      }
  }

   /** Suppression de la carte si l'on a plusieurs cartes lors de la suppression de la page */
   ngOnDestroy() {
    //this.map.remove();
  }

  /**
   * Avant que l'on affiche la page, on instancie la carte et on initialise les données
   */
  ionViewDidEnter() {
    this.leafletMap();
  }

  /**
   * Fonction d'initialisation de la carte LeafLet
   */
  leafletMap() {
    this.map = Leaflet.map('mapId').setView([46.5593567,2.364429], 6);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Céréma © Angular LeafLet',
    }).addTo(this.map);
    this.reinit();
  }

  /**
   * Fonction pour accèder à la page de connexion/déconnexion
   */
  connexion(){
    this.nav.navigateForward([`connexion`]);
  }

  /**
   * Fonction appelé par un click dans l'infobulle pour obtenir la liste des personnes d'un site
   *
   * @param param identifiant du site dont on veut afficher les agents
   */
  gotolien(param: string){
    const date = new Date(Date.now()).toJSON().substring(0, 10);
    this.nav.navigateForward([`tabs/tab2/${date}/${param}`]);
  }

  /**
   * Compare 2 objets service de la liste déroulante
   *
   * @param o1 premier élément de comparaison
   * @param o2 deuxième élément de comparaison
   * @returns Vrai si les deux objet sont identiques, Faux sinon
   */
  compareniveau(o1: number,o2: number){
    return o1 && o2 ? o1 === o2 : o1 === o2;
  }

  /**
   * Fonction de réinitialisation des éléments de la carte liée aux données
   */
  reinit(): void{
    if ( this.selectedService !== this.service ) {
      this.service = this.selectedService;
      let serv: string = 'DSI';
      this.services.forEach(element => {
        if ( element.niveau === this.service) { serv = element.libelle; }
      });
      const regex =/\//g;
      this.api.getCarto(serv.replace(regex, '-')).subscribe(x => {
        // suppression des markers
        let  iconSite: Leaflet.Icon;
        for ( const marki of this.markers) { marki.remove(); }
        this.markers = [];
        for ( const pos of x) {
          let texte: string = '';
          if ( pos.site === 0 ) {
            iconSite = this.aucunIcon;
            texte = 'Aucun agent DSI';
            }
          else if ( pos.bureau > 0 ) {
            iconSite = this.presentIcon;
            texte='Sur site: '+pos.bureau+'/'+pos.site;
          }
          else if ( pos.tele > 0 ) {
            iconSite = this.teleIcon;
            texte='Hors site: '+pos.tele+'/'+pos.site;
          }
          else {
            iconSite = this.absentIcon;
            texte='Ne pas déranger: '+pos.conge+'/'+pos.site;
          }
          const anchor = Leaflet.DomUtil.create('a');
          anchor.innerHTML=texte;
          anchor.setAttribute('id',pos.commune);
          if ( texte !== 'Aucun agent DSI' ) { anchor.addEventListener('click',(e: any)=>
            {
              const commune = e.target.getAttribute('id');
              this.gotolien(`${this.selectedService}/${commune}`);
            });
          }
          const mark = Leaflet.marker([pos.latitude, pos.longitude],{icon: iconSite});
          mark.addTo(this.map)
            .bindPopup(anchor);
          mark.bindTooltip(pos.commune);
          this.markers.push(mark);
        }
      });
    }
  }

  /**
   * Fonction demandant la reconnexion à Orion
   */
   orion(): void{
    if ( this.auth.isAuthenticated() ) {
      this.auth.logout();
    }
    else {
      this.auth.login();
    }
  }
}
