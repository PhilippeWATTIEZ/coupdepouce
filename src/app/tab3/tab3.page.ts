import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { NavController } from '@ionic/angular';
import { Mois } from '../models/mois';
import { ActivatedRoute } from '@angular/router';
import { Emails } from '../models/emails';
import { AuthenticationService } from '../services/authentication.service';
import { apiurl } from 'src/environments/environment';

/**
 * Composant associé à la page Onglet 3: mois
 */
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

/**
 * Classe associé à la page Onglet 3: mois
 */
export class Tab3Page implements OnInit{
  /** Tableau des positions par semaine du mois à afficher */
  semaines: Mois[];
  /** Titre du bandeau calculé en fonction de l'agent et du mois sélectionné */
  titre: string;
  /** Agent correspondant à l'utilisateur sélectionné */
  agent: Agent;
  /** Email de l'agent sélectionné pour l'affichage des données  */
  email: Emails;
  /** variable contenant le paramétre pour l'appel des api */
  param: string;
  /** variable associée au champ du formaulaire de recherche d'un agent  */
  searchTerm: string;
  /** Liste des agents correspondants au masque de recherche */
  emails: Emails[] = [];

  /**
   * Constructeur de la classe avec les services nécessaires à son fonctionnement
   *
   * @param api Service d'accès aux informations en backoffice via API REST
   * @param nav Service de controle de la navigation
   * @param user Service de stockage en locale des informations de connexion
   * @param activatedRoute Service de manipulation des informations de requêtes
   */
  constructor(public api: ApiserviceService,public nav: NavController,public user: AgentService,
      public activatedRoute: ActivatedRoute, private auth: AuthenticationService) {
    this.user.getObservable().subscribe(x => {
      this.agent = x;
      if ( x !== null && x !== undefined && x.email !== '') {
        this.api.getEmail(x.email).subscribe(y => {
          this.email = y;
          this.titre = this.email.agent;
        });
        this.api.getMois(x.email).subscribe(z => this.semaines = z);
      }
    });
  }

  /**
   * Fonction implémentée avec la classe pour initialiser les éléments de la page dans le cycle de vie
   *
   *
   * - vérification que l'utilisateur est connecté, sinon affichage de la page connexion
   * - initialisation de l'agent sinon l'utilisateur connecté
   * - calcul du titre à afficher dans le bandeau
   * - alimentation du tableau de résultats
   *
   */
  ngOnInit() {
    this.agent = this.user.getAgent();
    if ( this.activatedRoute.snapshot.paramMap.has('email')) { this.param = this.activatedRoute.snapshot.paramMap.get('email'); }
    else { this.param = this.agent===null?'':this.agent.email; }
    if (  this.agent !== null &&  this.agent !== undefined &&  this.agent.email !== '') {
        this.api.getEmail(this.param).subscribe(y => {
          this.email = y;
          this.titre = this.email.agent;
        });
        this.api.getMois(this.param).subscribe(z => this.semaines = z);
      }
    }

  /**
   * Fonction appelé lorsque l'on utilise le ion-refresher, elle reactualise les données avec celles en base de données
   *
   * @param event Evénement qui a déclenché l'appel de la fonction sur la page
   */
  doRefresh(event): void {
    this.api.getMois(this.param).subscribe(
      x => this.semaines = x,
      () => console.log('erreur de rechargement'),
      () => event.target.complete()
    );
  }

  /**
   * Fonction déclenchée par le choix d'un agent dans la liste des possibilités qui affiche les résultats en fonction du choix
   *
   * @param email Objet Emails qui a été cliqué dans la liste des agents correspondant au masque de recherche
   */
  choix(email: Emails): void{
    this.email=email;
    this.param = email.email;
    this.titre = email.agent;
    this.searchTerm='';
    this.emails=[];
    this.api.getMois(this.param).subscribe(x => {
      this.semaines = x;
    });
  }

  /**
   * Fonction déclenché à chaque modification de la zone de recherche agent,
   * raffraichit la liste des agents correspondnat si la chaine contient au moins 2 caractères
   */
  searchChanged(): void{
    if ( this.searchTerm.length > 2) {
      this.api.rechercheEmails(this.searchTerm.toLowerCase()).subscribe(x => this.emails = x);
    } else { this.emails = []; }
  }

  /**
   * Fonction qui permet d'accèder à la page de connexion/déconnexion
   */
  connexion(): void{
    this.nav.navigateForward([`connexion`]);
  }

  /**
   * Fonction qui permet de générer le tableau des congés d'un agent
   */
  recap(){
    window.open(apiurl+'recap/feuilleAgent.php?email='+this.param,'_blank');
  }

  /**
   * Fonction demandant la reconnexion à Orion
   */
   orion(): void{
    if ( this.auth.isAuthenticated() ) {
      this.auth.logout();
    }
    else {
      this.auth.login();
    }
  }
}
