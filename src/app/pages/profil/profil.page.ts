import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../../services/apiservice.service';
import { AgentService } from '../../services/agent.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Profil } from 'src/app/models/profil';

/**
 * Composant de la page affichant le profil d'un agent de l'organigramme
 */
@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})

/**
 * Classe associé à la page d'affichage d'un profil d'un agent de l'organigramme
 */
export class ProfilPage implements OnInit {
  /** Identifiant en cours d'affichage */
  id: string;
  /** Objet profil d'un agent */
  profil: Profil;

  /**
   * Constructeur de la classe avec les services nécessaires à sont fonctionnement
   *
   * @param api Service des API en base de données via des requete API REST
   * @param nav Service de navigation dans les pages
   * @param user Service de connexion de l'utilisateur
   * @param activatedRoute Service des routes utilisées pour arriver à la page en cours
   */
  constructor(public api: ApiserviceService,private nav: NavController,private user: AgentService,
    private activatedRoute: ActivatedRoute) {}

  /**
   * Fonction implémentée pour initialiser les données de la page
   *
   * Si pas de paramètre "id" pour accèder à la page, id = 1 puis alimentation des données avec l'agent ayant id pour identifiant
   *
   */
  ngOnInit() {
    if ( this.activatedRoute.snapshot.paramMap.has('id')) { this.id = this.activatedRoute.snapshot.paramMap.get('id'); }
    else { this.id = '1'; }
    this.api.getProfil(this.id).subscribe(x => this.profil = x);
  }

  /**
   * Fonction de retour à la page appelante
   */
  back(){
    this.nav.back();
  }
}
