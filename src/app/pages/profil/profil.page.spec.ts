import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiserviceService } from '../../services/apiservice.service';
import { AgentService } from '../../services/agent.service';
import { OAuthService, UrlHelperService, OAuthLogger,OAuthModule,DateTimeProvider } from 'angular-oauth2-oidc';
import { ProfilPage } from './profil.page';

describe('ProfilPage', () => {
  let component: ProfilPage;
  let fixture: ComponentFixture<ProfilPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilPage ],
      providers: [ApiserviceService,AgentService,OAuthService, UrlHelperService,DateTimeProvider, OAuthLogger],
      imports: [IonicModule.forRoot(),HttpClientTestingModule,RouterTestingModule,OAuthModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
