import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiserviceService } from '../../services/apiservice.service';
import { AgentService } from '../../services/agent.service';
import { BehaviorSubject } from 'rxjs';
import { Agent } from 'src/app/models/agent';
import { OAuthService, UrlHelperService, OAuthLogger,OAuthModule,DateTimeProvider } from 'angular-oauth2-oidc';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ConnexionPage } from './connexion.page';

describe('ConnexionPage', () => {
  let component: ConnexionPage;
  let fixture: ComponentFixture<ConnexionPage>;
  const agent: Agent = {
    id: '90',
    identifiant: 'a2e8ca99-890c-47a0-aca0-5c8e3d0477b1',
    agent: 'WATTIEZ Philippe',
    site: null,
    telephone: '+33 482917538',
    mobile: '+33 661649314',
    service: 'EPTN',
    fonction: 'Chef de pôle',
    email: 'philippe.wattiez@cerema.fr',
    complet: 'DSI/SGI/EPTN',
    designation: 'Pôle études, projets et transformation numérique',
    trigramme: 'PWA'
  };
  const listeEmail: string[] = ['xxxxx@cerema.fr','yyyyyyy@cerema.fr','zzzzzzzz@cerema.fr','wwwwwww@cerema.fr'];
  let title: HTMLElement;


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnexionPage ],
      providers: [ApiserviceService,AuthenticationService, AgentService,OAuthService, UrlHelperService,DateTimeProvider,OAuthLogger],
      imports: [IonicModule.forRoot(),HttpClientTestingModule,RouterTestingModule,OAuthModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConnexionPage);
    component = fixture.componentInstance;
    component.user = TestBed.inject(AgentService);
    component.api = TestBed.inject(ApiserviceService);
    component.api.searchEmails = (email: string) => {
      const ret = new BehaviorSubject(listeEmail);
      return ret;
    };
    component.user.setAgent(agent);
  }));

  afterEach(() => {
    component.user.videStorage().then(() => console.log('Locale storage vidé'));
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should display "Droit d\'utilisation" when nobody is connected', () => {
    fixture.detectChanges();
    title =  fixture.nativeElement.querySelector('ion-title');
    expect(title.textContent).toContain('Droit d\'utilisation');
  });

/*
  it('should display the name of the connected agent after change detected', () => {
    component.agent = agent;
    //component.ngOnInit();
    fixture.detectChanges();
    title =  fixture.nativeElement.querySelector('ion-title');
    expect(title.textContent).toContain('WATTIEZ Philippe');
  });


  it('method deconnecte() emptying de local store', () => {
    component.agent = agent;
    expect(component.agent).toEqual(agent);
    component.deconnecte();
    expect(component.agent).toBeNull();
  });
  */
});
