import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../../services/apiservice.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavController } from '@ionic/angular';
import { Agent } from 'src/app/models/agent';
import { AgentService } from '../../services/agent.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { authCodeFlowConfig } from 'src/environments/environment';

/**
 * Composant connexion/déconnexion/visualisation du profil
 */
@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})

/**
 * Classe associée à la page de connexion
 */
export class ConnexionPage implements OnInit {
  /** valeur de la clé de l'agent  */
  public cle: string = '';

  /** Objet Agent correspondant à l'utilisateur connecté */
  public agent: Agent;

  /** Objet compte utilisateur du token Orion */
  public claims: any = null;

  /**
   * Contructeur de la classe avec les services nécessaires à son fonctionnement
   *
   * @param api Service d'accès aux informations en backoffice via API REST
   * @param user Service de stockage en locale des informations de connexion
   * @param nav Service de controle de la navigation
   */
  constructor(public api: ApiserviceService, public user: AgentService,private nav: NavController,
    private auth: AuthenticationService, private oauthService: OAuthService) { }

  /**
   * Initialisation de la page avec les informations locales de connexion si elles existent
   */
  ngOnInit() {
    console.log('entrée dans onInit connexion');
    this.agent= this.user.getAgent();
    console.log(this.agent);
    console.log(this.auth.isAuthenticated());
    if ( ! this.auth.isAuthenticated()) {
      this.oauthService.configure(authCodeFlowConfig);
      this.oauthService.loadDiscoveryDocumentAndTryLogin().then(ret => {
        if ( ret) {
          console.log(this.oauthService.hasValidIdToken());
          this.claims = this.oauthService.getIdentityClaims();
          if ( this.claims) {
            console.log('-- login claims --', this.claims);
            this.api.getUser(this.claims.email).subscribe( x => {
              this.user.setAgent(x);
              this.auth.authState.next(true);
              this.back();
            });
          }
        }
      });
    } else {
      if ( this.agent !== null ) {
        this.claims= {email: this.agent.email};
      }
    }
    //this.token();
  }


  /**
   * Fonction pour revenir à la page précédente à l'appel de la page de connexion ou de consultation du profil
   *
   * @returns
   */
  back(){
    this.nav.navigateRoot('/tabs/tab1');
  }

  /**
   * Fonction déclenchée par le bouton déconnexion
   *
   *
   * Vide les information locale de connexion
   *
   */
  deconnecte(){
    this.auth.logout();
  }

  /**
   * Fonction déclenchée par le bouton déconnexion
   *
   *
   * Vide les information locale de connexion
   *
   */
   connecte(){
    console.log('entrée dans login()');
    //this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.initCodeFlow();
  }

  /**
   * getter de l'attribut token
   *
   * Renvoi null si Orion ne peut identifier le compte associé par le token sinon l'utilsateur correspondant au jeton
   *
   */
  token() {
    console.log('entrée dans token');
    if ( ! this.auth.isAuthenticated()) {
      this.claims = this.oauthService.getIdentityClaims();
      if ( this.claims) {
        console.log('-- login claims --', this.claims);
        this.api.getUser(this.claims.email).subscribe( x => {
          this.user.setAgent(x);
          this.auth.authState.next(true);
          this.back();
        });
      }
    }
  }
}
