import { Injectable } from '@angular/core';
import { Router,ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

/**
 * Injection
 */
@Injectable({
  providedIn: 'root'
})

/**
 * Classe permettant de controler les routes pouvant être obtenue si on est authentifié ou pas
 */
export class AuthGuard implements CanActivate {
  /**
   * Constructeur de la classe nécesstant l'accès au service d'authentification et la construction d'une route vers la connsion
   *
   * @param router routeur pour changer d'url de destination
   * @param auth service d'authentification
   */
  constructor(private router: Router, private auth: AuthenticationService) {}

  /**
   * Méthode permettant de permettre une route d'être activé ou remplacé par une autre
   *
   * @param route route qui doit être activé ou pas
   * @param state état des routes
   * @returns false si l'utilsatuer est connecté sur Orion sinon la route vers la page de connexion
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if ( this.auth.isAuthenticated()) {
          return true;
        }
        const urlTree = this.router.createUrlTree(['connexion']);
        return urlTree;
  }
}
