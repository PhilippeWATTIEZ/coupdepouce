/**
 * Classe représentant les sites du Céréma avec les informations provenant d'ASPRO
 */
export class Sites {
    /** Nom de la commune où est situé le site */
    commune: string;
    /** Latitude permettant de géolocaliser le site */
    latitude: number;
    /** Longitude permettant de géolocaliser le site */
    longitude: number;
    /** Nombre de personnes total du site */
    site: number;
    /** Nombre de personnes du site en télétravail actuellement */
    tele: number;
    /** Nombre de personnes du site à ne pas déranger car en congés */
    conge: number;
    /** Nombre de personnes sur site actuellement */
    bureau: number;

    /**
     * Constructeur de la classe Sites à partir des éléments en paramètre et les convertit
     *
     * @param commune Nom de la commune où est situé le site
     * @param latitude Latitude permettant de géolocaliser le site
     * @param longitude Longitude permettant de géolocaliser le site
     * @param site Nombre de personnes total du site
     * @param tele Nombre de personnes du site en télétravail actuellement
     * @param conge Nombre de personnes du site à ne pas déranger car en congés
     * @param bureau Nombre de personnes sur site actuellement
     */
     constructor(commune: string,latitude: string,longitude: string,site: string,
        tele: string,conge: string,bureau: string) {
        this.commune = commune;
        this.latitude = parseFloat(latitude);
        this.longitude = parseFloat(longitude);
        this.site = parseInt(site,10);
        this.tele = parseInt(tele,10);
        this.conge = parseInt(conge,10);
        this.bureau = parseInt(bureau,10);
    }

    /**
     * Fonction statique permettant la création d'un instance de Sites à partir d'un objet JSON
     *
     * @param json Objet json transmis suite à une requete HTTP API REST
     * @returns un objet de la classe Agents alimenté à partir des attributs JSON
     */
    public static fromJson(json): Sites {
        return new Sites(
            json.commune,
            json.latitude,
            json.longitude,
            json.site,
            json.tele,
            json.conge,
            json.bureau
            );
    }
}

