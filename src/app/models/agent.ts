/**
 * Classe des agents du Céréma selon alimentation aspro
 *
 * Plus d'info en suivant le lien: https://aspro.cerema.fr/agents
 */
export class Agent {
	/** N° identifiant unique de l'agent */
    id: string;
	/** Identifiant ASPRO de l'agent de type "a2e8ca99-890c-47a0-aca0-5c8e3d0477b1"*/
	identifiant: string;
	/** Nom et Prénom de l'agent */
	agent: string;
	/** Site où est localisé l'agent (exemple BRON, LILLE, METZ, ...) */
	site: string;
	/** N° de téléphone professionnel de l'agent */
	telephone: string;
	/** N° de téléphone mobile professionnel de l'agent */
	mobile: string;
	/** Service de l'agent, exemple EPTN */
	service: string;
	/** fonction de l'agent (optionnel) */
	fonction: string;
	/** Service de l'agent, complet, exemple DSI/SGI/EPTN */
	complet: string;
	/** Email de l'agent, en @cerema.fr */
	email: string;
	/** Désignation du service (optionnel) */
	designation: string;
	/** Trigramme composé de l'initiale du prénom suivi des 2 premières lettres du nom */
	trigramme: string;
}
