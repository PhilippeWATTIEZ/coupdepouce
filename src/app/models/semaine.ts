/**
 * Classe d'objet représentant une semaine pour un agent d'un service
 */
export class Semaine {
    /** Email de l'agent */
    email: string;
    /** Désignation longue du service (optionnel) */
    designation: string;
    /** Nom et Prénom de l'agent */
    agent: string;
    /** Nom de l'agent */
    nom: string;
    /** Nombre du service, permettant le filtrage en ne prenant que les nb multiple de niveau (1 = toute la DSI) */
    niveau: number;
    /** N° d'ordre dans l'organigramme */
    tri: string;
    /** code position au lundi matin */
    matin1: string;
    /** code position au lundi soir */
    soir1: string;
    /** code demande position au lundi matin */
    demmatin1: string;
    /** code demande position au lundi soir */
    demsoir1: string;
    /** code position au mardi matin */
    matin2: string;
    /** code position au mardi soir */
    soir2: string;
    /** code demande position au mardi matin */
    demmatin2: string;
    /** code demande position au mardi soir */
    demsoir2: string;
    /** code position au mercredi matin */
    matin3: string;
    /** code position au mercredi soir */
    soir3: string;
    /** code demande position au mercredi matin */
    demmatin3: string;
    /** code demande position au mercredi soir */
    demsoir3: string;
    /** code position au jeudi matin */
    matin4: string;
    /** code position au jeudi soir */
    soir4: string;
    /** code demande position au jeudi matin */
    demmatin4: string;
    /** code demande position au jeudi soir */
    demsoir4: string;
    /** code position au vendredi matin */
    matin5: string;
    /** code position au vendredi soir */
    soir5: string;
    /** code demande position au vendredi matin */
    demmatin5: string;
    /** code demande position au vendredi soir */
    demsoir5: string;
    /** libellé calculé de la position au lundi matin */
    m1: string;
    /** libellé calculé de la position au lundi soir */
    s1: string;
    /** libellé calculé de la position au mardi matin */
    m2: string;
    /** libellé calculé de la position au mardi soir */
    s2: string;
    /** libellé calculé de la position au mercredi matin */
    m3: string;
    /** libellé calculé de la position au mercredi soir */
    s3: string;
    /** libellé calculé de la position au jeudi matin */
    m4: string;
    /** libellé calculé de la position au jeudi soir */
    s4: string;
    /** libellé calculé de la position au vendredi matin */
    m5: string;
    /** libellé calculé de la position au vendredi soir */
    s5: string;
    /** Couleur pour distinguer agent et chef de service */
    color: string;

    /**
     * Construteur de la classe avec des paramétre d'initisialisation des propriétés
     *
     * @param email Email de l'agent
     * @param designation Désignation longue du service (optionnel)
     * @param agent Nom et Prénom de l'agent
     * @param nom Nom de l'agent
     * @param niveau Nombre du service, permettant le filtrage en ne prenant que les nb multiple de niveau (1 = toute la DSI)
     * @param tri N° d'ordre dans l'organigramme
     * @param matin1 code position au lundi matin
     * @param soir1 code position au lundi soir
     * @param matin2 code position au mardi matin
     * @param soir2 code position au mardi soir
     * @param matin3 code position au mercredi matin
     * @param soir3 code position au mercredi soir
     * @param matin4 code position au jeudi matin
     * @param soir4 code position au jeudi soir
     * @param matin5 code position au vendredi matin
     * @param soir5 code position au vendredi soir
     * @param demmatin1  code demande position au lundi matin
     * @param demsoir1  code demande position au lundi soir
     * @param demmatin2  code demande position au mardi matin
     * @param demsoir2  code demande position au mardi soir
     * @param demmatin3  code demande position au mercredi matin
     * @param demsoir3  code demande position au mercredi soir
     * @param demmatin4  code demande position au jeudi matin
     * @param demsoir4  code demande position au jeudi soir
     * @param demmatin5  code demande position au vendredi matin
     * @param demsoir5  code demande position au vendredi soir
     * @param color Couleur pour distinguer agent et chef de service
     */
     constructor(email: string,
        designation: string,
        agent: string,
        nom: string,
        niveau: string,
        tri: string,
        matin1: string,
        soir1: string,
        matin2: string,
        soir2: string,
        matin3: string,
        soir3: string,
        matin4: string,
        soir4: string,
        matin5: string,
        soir5: string,
        demmatin1: string,
        demsoir1: string,
        demmatin2: string,
        demsoir2: string,
        demmatin3: string,
        demsoir3: string,
        demmatin4: string,
        demsoir4: string,
        demmatin5: string,
        demsoir5: string,
        color: string) {
        this.email = email;
        this.designation = designation;
        this.agent = agent;
        this.nom = nom;
        this.niveau = parseInt(niveau,10);
        this.tri = tri;
        this.matin1 = matin1;
        this.soir1 = soir1;
        this.matin2 = matin2;
        this.soir2 = soir2;
        this.matin3 = matin3;
        this.soir3 = soir3;
        this.matin4 = matin4;
        this.soir4 = soir4;
        this.matin5 = matin5;
        this.soir5 = soir5;
        this.m1=Semaine.conv(matin1);
        this.m2=Semaine.conv(matin2);
        this.m3=Semaine.conv(matin3);
        this.m4=Semaine.conv(matin4);
        this.m5=Semaine.conv(matin5);
        this.s1=Semaine.conv(soir1);
        this.s2=Semaine.conv(soir2);
        this.s3=Semaine.conv(soir3);
        this.s4=Semaine.conv(soir4);
        this.s5=Semaine.conv(soir5);
        this.demmatin1 = demmatin1;
        this.demsoir1 = demsoir1;
        this.demmatin2 = demmatin2;
        this.demsoir2 = demsoir2;
        this.demmatin3 = demmatin3;
        this.demsoir3 = demsoir3;
        this.demmatin4 = demmatin4;
        this.demsoir4 = demsoir4;
        this.demmatin5 = demmatin5;
        this.demsoir5 = demsoir5;
        this.color = color;
    }

    /**
     * Fonction statique de classe permettant de créer une instance de la classe semaine à partir
     * d'un objet JSON issu d'une requete HTTP REST API
     *
     * @param json Objet JSON issu d'une requete HTTP REST API
     * @returns Instance de classe Semaine alimentée par les attributs de l'objet JSON
     */
    public static fromJson(json): Semaine {
        return new Semaine(
            json.email,
            json.designation,
            json.agent,
            json.nom,
            json.niveau,
            json.tri,
            json.matin1,
            json.soir1,
            json.matin2,
            json.soir2,
            json.matin3,
            json.soir3,
            json.matin4,
            json.soir4,
            json.matin5,
            json.soir5,
            json.demmatin1,
            json.demsoir1,
            json.demmatin2,
            json.demsoir2,
            json.demmatin3,
            json.demsoir3,
            json.demmatin4,
            json.demsoir4,
            json.demmatin5,
            json.demsoir5,
            json.color
           );
    }

    /**
     * Fonction statique de classe pour convertir les codes de position en libellés intelligibles:
     *
     *
     *               T  => Mobilisable sur site
     *               TT => Mobilisable hors site
     *               C  => Ne pas déranger
     *               Sinon => Inconnu
     *
     * @param val Valeur du code à convertir
     * @returns Libellé long correspondant à afficher
     */
    public static conv(val: string): string {
        switch (val) {
            case 'T':
              return 'Mobilisable sur site';
        case 'TT':
            return 'Mobilisable hors site';
        case 'C':
            return 'Ne pas déranger';
        default:
            return 'Inconnu';
        }
    }
}
