import { Sites } from './sites';

describe('Sites', () => {
  it('should create an instance', () => {
    expect(new Sites('AIX-EN-PROVENCE','43.492798','5.376806','1','0','0','1')).toBeTruthy();
  });
  it('should create an instance from a JSON Object', () => {
    const agent = new Sites('AIX-EN-PROVENCE','43.492798','5.376806','1','0','0','1');
    const json = JSON.parse('{"commune":"AIX-EN-PROVENCE","latitude":"43.492798",'+
      '"longitude":"5.376806","site":"1","tele":"0","conge":"0","bureau":"1"}');
    expect(Sites.fromJson(json)).toEqual(agent);
  });

});
