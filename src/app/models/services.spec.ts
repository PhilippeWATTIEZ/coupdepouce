import { Services } from './services';

describe('Services', () => {
  it('should create an instance', () => {
    expect(new Services('SSN','Service stratégie numérique','DSI',
      '8152b744-62d3-102c-8998-934935960c30','67','light','DSI/SSN')).toBeTruthy();
  });

  it('should create an instance from a JSON Object', () => {
    const agent = new Services('SSN','Service stratégie numérique','DSI',
      '8152b744-62d3-102c-8998-934935960c30','67','light','DSI/SSN');
    const json = JSON.parse('{"sigle":"SSN","rattachement":"DSI",'+
      '"designation":"Service strat\u00e9gie num\u00e9rique",'+
      '"responsable":"8152b744-62d3-102c-8998-934935960c30","niveau":"67","color":"light",'+
      '"chemin":"DSI\/SSN"}');
    expect(Services.fromJson(json)).toEqual(agent);
  });

});
