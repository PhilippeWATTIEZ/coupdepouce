/**
 * Classe du profil agent connecté
 */
export class Profil {
    /** Nom et Prénom de l'agent */
    agent: string;
    /** Site où est localisé l'agent (exemple BRON, LILLE, METZ, ...) */
    site: string;
    /** N° de téléphone professionnel de l'agent */
    telephone: string;
    /** N° de téléphone mobile professionnel de l'agent */
    mobile: string;
    /** Sigle du  service de l'agent, exemple EPTN */
    service: string;
    /** Fonction de l'agent, optionnel */
    fonction: string;
    /** Email de l'agent, en @cerema.fr */
    email: string;
    /** Sigle du  service de l'agent, complet, exemple DSI/SGI/EPTN */
    complet: string;
    /** Sigle du  service de l'agent, optionnel */
    designation: string;
}
