/**
 * Classe d'un mois de position pour un agent
 */
export class Mois {
    /** Nom et Prénom de l'agent */
    agent: string;
    /** Nom de l'agent */
    nom: string;
    /** Email de l'agent */
    email: string;
    /** Date permettant de fixer la semaine (le lundi) */
    date: string;
    /** semaine du dd/mm/yyyy au dd/mm/yyyy */
    semaine: string;
    /** code position au lundi matin */
    matin1: string;
    /** code position au lundi soir */
    soir1: string;
    /** code demande position au lundi matin */
    demmatin1: string;
    /** code demande position au lundi soir */
    demsoir1: string;
    /** Couleur du mois permettant de repérer la rupture de mois dans une semaine */
    color1: string;
    /** code position au mardi matin */
    matin2: string;
    /** code position au mardi soir */
    soir2: string;
    /** code demande position au mardi matin */
    demmatin2: string;
    /** code demande position au mardi soir */
    demsoir2: string;
    /** Couleur du mois permettant de repérer la rupture de mois dans une semaine */
    color2: string;
    /** code position au mercredi matin */
    matin3: string;
    /** code position au mercredi soir */
    soir3: string;
    /** code demande position au mercredi matin */
    demmatin3: string;
    /** code demande position au mercredi soir */
    demsoir3: string;
    /** Couleur du mois permettant de repérer la rupture de mois dans une semaine */
    color3: string;
    /** code position au jeudi matin */
    matin4: string;
    /** code position au jeudi soir */
    soir4: string;
    /** code demande position au jeudi matin */
    demmatin4: string;
    /** code demande position au jeudi soir */
    demsoir4: string;
    /** Couleur du mois permettant de repérer la rupture de mois dans une semaine */
    color4: string;
    /** code position au vendredi matin */
    matin5: string;
    /** code position au vendredi soir */
    soir5: string;
    /** code demande position au vendredi matin */
    demmatin5: string;
    /** code demande position au vendredi soir */
    demsoir5: string;
    /** Couleur du mois permettant de repérer la rupture de mois dans une semaine */
    color5: string;
    /** libellé calculé de la position au lundi matin */
    m1: string;
    /** libellé calculé de la position au lundi soir */
    s1: string;
    /** libellé calculé de la position au mardi matin */
    m2: string;
    /** libellé calculé de la position au mardi soir */
    s2: string;
    /** libellé calculé de la position au mercredi matin */
    m3: string;
    /** libellé calculé de la position au mercredi soir */
    s3: string;
    /** libellé calculé de la position au jeudi matin */
    m4: string;
    /** libellé calculé de la position au jeudi soir */
    s4: string;
    /** libellé calculé de la position au vendredi matin */
    m5: string;
    /** libellé calculé de la position au vendredi soir */
    s5: string;


    /**
     * Constructeur de la classe mois avec paramétre d'initalisation
     *
     * @param agent Nom et Prénom de l'agent
     * @param nom  Nom de l'agent
     * @param email Email de l'agent
     * @param date Date permettant de fixer la semaine (le lundi)
     * @param semaine semaine du dd/mm/yyyy au dd/mm/yyyy
     * @param matin1 code position au lundi matin
     * @param soir1 code position au lundi soir
     * @param demmatin1 code demande position au lundi matin
     * @param demsoir1 code demande position au lundi soir
     * @param color1  Couleur du mois permettant de repérer la rupture de mois dans une semaine
     * @param matin2 code position au mardi matin
     * @param soir2 code position au mardi soir
     * @param demmatin2 code demande position au mardi matin
     * @param demsoir2 code demande position au mardi soir
     * @param color2 Couleur du mois permettant de repérer la rupture de mois dans une semaine
     * @param matin3 code position au mercredi matin
     * @param soir3 code position au mercredi soir
     * @param demmatin3 code demande position au mercredi matin
     * @param demsoir3 code demande position au mercredi soir
     * @param color3  Couleur du mois permettant de repérer la rupture de mois dans une semaine
     * @param matin4  code position au jeudi matin
     * @param soir4 code position au jeudi soir
     * @param demmatin4 code demande position au jeudi matin
     * @param demsoir4 code demande position au jeudi soir
     * @param color4 Couleur du mois permettant de repérer la rupture de mois dans une semaine
     * @param matin5 code position au vendredi matin
     * @param soir5 code position au vendredi soir
     * @param demmatin5 code demande position au vendredi matin
     * @param demsoir5 code demande position au vendredi soir
     * @param color5 Couleur du mois permettant de repérer la rupture de mois dans une semaine
     */
     constructor(agent: string, nom: string, email: string, date: string, semaine: string,
            matin1: string, soir1: string, demmatin1: string, demsoir1: string, color1: string,
            matin2: string, soir2: string, demmatin2: string, demsoir2: string, color2: string,
            matin3: string, soir3: string, demmatin3: string, demsoir3: string, color3: string,
            matin4: string, soir4: string, demmatin4: string, demsoir4: string, color4: string,
            matin5: string, soir5: string, demmatin5: string, demsoir5: string, color5: string) {
        this.nom = nom;
        this.email = email;
        this.date = date;
        this.semaine = semaine;
        this.matin1 = matin1;
        this.soir1 = soir1;
        this.demmatin1 = demmatin1;
        this.demsoir1 = demsoir1;
        this.color1 = color1;
        this.matin2 = matin2;
        this.soir2 = soir2;
        this.demmatin2 = demmatin2;
        this.demsoir2 = demsoir2;
        this.color2 = color2;
        this.matin3 = matin3;
        this.soir3 = soir3;
        this.demmatin3 = demmatin3;
        this.demsoir3 = demsoir3;
        this.color3 = color3;
        this.matin4 = matin4;
        this.soir4 = soir4;
        this.demmatin4 = demmatin4;
        this.demsoir4 = demsoir4;
        this.color4 = color4;
        this.matin5 = matin5;
        this.soir5 = soir5;
        this.demmatin5 = demmatin5;
        this.demsoir5 = demsoir5;
        this.color5 = color5;
        this.m1=Mois.conv(matin1);
        this.m2=Mois.conv(matin2);
        this.m3=Mois.conv(matin3);
        this.m4=Mois.conv(matin4);
        this.m5=Mois.conv(matin5);
        this.s1=Mois.conv(soir1);
        this.s2=Mois.conv(soir2);
        this.s3=Mois.conv(soir3);
        this.s4=Mois.conv(soir4);
        this.s5=Mois.conv(soir5);
    }

    /**
     * Fonction statique de classe permettant de créer une instance de classe à partir d'un objet JSON issu d'une requete HTTP REST API
     *
     * @param json objet JSON issu d'une requete HTTP REST API
     * @returns Une instance de la classe Mois alimenté avec les attribut de l'objet JSON
     */
    public static fromJson(json): Mois {
        return new Mois(
            json.agent,json.nom,json.email,json.date,json.semaine,json.matin1,json.soir1,
            json.demmatin1,json.demsoir1,json.color1,json.matin2,json.soir2,json.demmatin2,
            json.demsoir2,json.color2,json.matin3,json.soir3,json.demmatin3,json.demsoir3,
            json.color3,json.matin4,json.soir4,json.demmatin4,json.demsoir4,json.color4,
            json.matin5,json.soir5,json.demmatin5,json.demsoir5,json.color5
           );
    }

    /**
     * Fonction statique de classe pour convertir les codes de position en libellés intelligibles:
     *
     *
     *               T  => Mobilisable sur site
     *               TT => Mobilisable hors site
     *               C  => Ne pas déranger
     *               Sinon => Inconnu
     *
     * @param val Valeur du code à convertir
     * @returns Libellé long correspondant à afficher
     */
    public static conv(val: string): string {
        switch (val) {
            case 'T':
              return 'Mobilisable sur site';
        case 'TT':
            return 'Mobilisable hors site';
        case 'C':
            return 'Ne pas déranger';
        default:
            return 'Inconnu';
        }
    }
}
