/**
 * Classe des emails correspondant à un critère de recherche
 */
export class Emails {
    /** Email dans la liste des agents correspondant à un critère de recherche  */
    email: string;
    /** Nom Prénom de l'agent */
    agent: string;
    /** Nom de l'agent */
    nom: string;

    /**
     * Constructeur de la classe Emails avec des valeurs fournies en paramétres
     *
     * @param email Email dans la liste des agents correspondant à un critère de recherche
     * @param agent Nom Prénom de l'agent
     * @param nom Nom de l'agent
     */
     constructor( email: string, agent: string, nom: string) {
        this.email = email;
        this.agent = agent;
        this.nom = nom;
    }

    /**
     * Fonction statique de la classe permettant de créer une instance à partir d'un Objet JSON
     *
     * @param json Objet JSON créer à partir d'une réponse HTTP REST API
     * @returns Une instance de la classe Emails alimenté avec les attributs de l'objet JSON
     */
    public static fromJson(json): Emails {
        return new Emails(
            json.email,
            json.agent,
            json.nom
           );
    }
}
