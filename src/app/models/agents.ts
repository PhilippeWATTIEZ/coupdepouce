/**
 * Classe pour la liste d'agents pour organigramme et présence le jour même
 */
export class Agents {
    /** N° identifiant unique de l'agent */
    id: string;
    /** Email de l'agent (en @cerema.fr) */
    email: string;
    /** Sigle court du service sans le chemin complet (exemple EPTN) */
    sigle: string;
    /** Désignation longue du service (optionnel) */
    designation: string;
    /** sigle du niveau supérieur du service (exemple SGI)   */
    rattachement: string;
    /** Nom et Prénom de l'agent */
    agent: string;
    /** Libellé du poste (optionnel)  */
    poste: string;
    /** Nom du site de l'agent (exemple BRON, LILLE, METZ, ...) */
    site: string;
    /** Nombre multiple du service, permettant le filtrage */
    nb: number;
    /** N° d'ordre dans l'organigramme */
    tri: string;
    /** détermine s'il est visible ou pas en fonction que son service est déplié ou replié */
    cache: boolean = false;
    /** icone affiché s'il est visible ou pas en fonction que son service est déplié ou replié */
    icone: string ='caret-down-outline';
    /** valeur par défaut de nb, se détermine s'il est négatif ou positif */
    defaut: number;
    /** Couleur de rattachement au service (visuel de l'organigramme) */
    color: string;
    /** Code position du matin pour aujourd'hui */
    matin: string;
    /** Code position du soir pour aujourd'hui */
    soir: string;
    /** Libellé position du matin pour aujourd'hui */
    lmatin: string;
    /** Libellé position du soir pour aujourd'hui */
    lsoir: string;

    /**
     * Constructeur de la classe Agents à partir des éléments en base, calcule les autres champs non transmis
     *
     * @param id N° identifiant unique de l'agent
     * @param email Email de l'agent (en @cerema.fr)
     * @param sigle Sigle court du service sans le chemin complet
     * @param designation Désignation longue du service (optionnel)
     * @param rattachement sigle du niveau supérieur du service
     * @param agent Nom et Prénom de l'agent
     * @param poste Libellé du poste (optionnel)
     * @param site Nom du site de l'agent
     * @param nb Nombre multiple du service, permettant le filtrage
     * @param tri N° d'ordre dans l'organigramme
     * @param color Couleur de rattachement au service
     * @param matin Position du matin pour aujourd'hui
     * @param soir Position du soir pour aujourd'hui
     */
    constructor( id: string,email: string, sigle: string,designation: string,rattachement: string,agent: string,poste: string,site: string,
        nb: string,tri: string,color: string, matin: string,soir: string) {
        this.id = id;
        this.email = email;
        this.sigle = sigle;
        this.designation = designation;
        this.rattachement = rattachement;
        this.agent = agent;
        this.nb = parseInt(nb,10);
        this.poste = poste;
        this.site = site;
        this.color = color;
        this.tri = tri;
        if (designation === '' && this.nb > 0)  {this.cache = true;}
        if (this.nb < 0)  {this.icone = 'caret-up-outline';}
        this.defaut = this.nb;
        this.matin = matin;
        this.soir = soir;
        this.lmatin=Agents.conv(matin);
        this.lsoir=Agents.conv(soir);
    }

    /**
     * Fonction statique de classe pour convertir les codes de position en libellés intelligibles:
     *
     *
     * - T  => Mobilisable sur site
     * - TT => Mobilisable hors site
     * - C  => Ne pas déranger
     * - Sinon => Inconnu
     *
     * @param val Valeur du code à convertir
     * @returns Libellé long correspondant à afficher
     */
     public static conv(val: string): string {
        switch (val) {
            case 'T':
              return 'Mobilisable sur site';
        case 'TT':
            return 'Mobilisable hors site';
        case 'C':
            return 'Ne pas déranger';
        default:
            return 'Inconnu';
        }
    }

    /**
     * Fonction statique de classe permettant de transformer une response HTTP en JSON en une instance de la classe Agents
     *
     * @param json Objet json transmis suite à une requete HTTP API REST
     * @returns un objet de la classe Agents alimenté à partir des attributs JSON
     */
    public static fromJson(json): Agents {
        return new Agents(
            json.id,
            json.email,
            json.sigle,
            json.designation,
            json.rattachement,
            json.agent,
            json.poste,
            json.site,
            json.nb,
            json.tri,
            json.color,
            json.matin,
            json.soir
           );
    }
}
