import { Agents } from './agents';

describe('Agents', () => {
  it('should create an instance', () => {
    expect(new Agents('90','philippe.wattiez@cerema.fr',
    'EPTN','Pôle études, projets et transformation numérique',
    'SGI','WATTIEZ Philippe','Chef de pôle','Bron', '1147','33',
    'success', 'C','C')).toBeTruthy();
  });

  it('should create an instance from a JSON Object', () => {
    const agent = new Agents('90','philippe.wattiez@cerema.fr', 'EPTN',
      'Pôle études, projets et transformation numérique','SGI','WATTIEZ Philippe',
      'Chef de pôle','Bron', '1147','33','success', 'C','C');
    const json = JSON.parse('{"email":"philippe.wattiez@cerema.fr","id":"90","sigle":"EPTN"'+
      ',"designation":"P\u00f4le \u00e9tudes, projets et transformation num\u00e9rique",'+
      '"rattachement":"SGI","agent":"WATTIEZ Philippe","poste":"Chef de p\u00f4le",'+
      '"site":"Bron","color":"success","nb":"1147","tri":"33","matin":"C","soir":"C"}');
    expect(Agents.fromJson(json)).toEqual(agent);
  });

  it('should static function conv convert like previous: T  => Mobilisable sur site', () => {
    expect(Agents.conv('T')).toEqual('Mobilisable sur site');
  });
  it('should static function conv convert like previous: TT => Mobilisable hors site', () => {
    expect(Agents.conv('TT')).toEqual('Mobilisable hors site');
  });
  it('should static function conv convert like previous: C  => Ne pas déranger', () => {
    expect(Agents.conv('C')).toEqual('Ne pas déranger');
  });
  it('should static function conv convert like previous: else => Inconnu', () => {
    expect(Agents.conv('?')).toEqual('Inconnu');
  });

});
