import { Emails } from './emails';

describe('Emails', () => {
  it('should create an instance', () => {
    expect(new Emails('philippe.wattiez@cerema.fr','WATTIEZ Philippe','WATTIEZ')).toBeTruthy();
  });

  it('should create an instance from a JSON Object', () => {
    const email = new Emails('philippe.wattiez@cerema.fr','WATTIEZ Philippe','WATTIEZ');
    const json = JSON.parse('{"email":"philippe.wattiez@cerema.fr","agent":"WATTIEZ Philippe","nom":"WATTIEZ"}');
    expect(Emails.fromJson(json)).toEqual(email);
  });
});
