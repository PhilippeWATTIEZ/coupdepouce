import { Semaine } from './semaine';

describe('Semaine', () => {
  it('should create an instance', () => {
    expect(new Semaine('jean-pierre.troeira@cerema.fr','Direction','TROEIRA Jean-Pierre','TROEIRA',
      '11','1','T','T','Rien','Rien','T','T','Rien','Rien','T','T','Rien','Rien','T','T','Rien',
      'Rien','T','T','Rien','Rien','medium')).toBeTruthy();
  });


  it('should create an instance from a JSON Object', () => {
    const agent = new Semaine('jean-pierre.troeira@cerema.fr','Direction','TROEIRA Jean-Pierre',
      'TROEIRA','11','1','T','T','T','T','T','T','T','T','T','T','Rien','Rien','Rien','Rien','Rien',
      'Rien','Rien','Rien','Rien','Rien','medium');
    const json = JSON.parse('{"designation":"Direction","niveau":"11","agent":"TROEIRA Jean-Pierre",'+
      '"nom":"TROEIRA","tri":"1","email":"jean-pierre.troeira@cerema.fr","color":"medium",'+
      '"matin1":"T","soir1":"T","demmatin1":"Rien","demsoir1":"Rien","matin2":"T","soir2":"T",'+
      '"demmatin2":"Rien","demsoir2":"Rien","matin3":"T","soir3":"T","demmatin3":"Rien",'+
      '"demsoir3":"Rien","matin4":"T","soir4":"T","demmatin4":"Rien","demsoir4":"Rien","matin5":"T",'+
      '"soir5":"T","demmatin5":"Rien","demsoir5":"Rien"}');
    expect(Semaine.fromJson(json)).toEqual(agent);
  });

  it('should static function conv convert like previous: T  => Mobilisable sur site', () => {
    expect(Semaine.conv('T')).toEqual('Mobilisable sur site');
  });
  it('should static function conv convert like previous: TT => Mobilisable hors site', () => {
    expect(Semaine.conv('TT')).toEqual('Mobilisable hors site');
  });
  it('should static function conv convert like previous: C  => Ne pas déranger', () => {
    expect(Semaine.conv('C')).toEqual('Ne pas déranger');
  });
  it('should static function conv convert like previous: else => Inconnu', () => {
    expect(Semaine.conv('?')).toEqual('Inconnu');
  });

});
