import { Mois } from './mois';

describe('Mois', () => {
  it('should create an instance', () => {
    expect(new Mois('agent', 'nom', 'email', 'date', 'semaine', 'matin1', 'soir1', 'demmatin1',
      'demsoir1', 'color1', 'matin2', 'soir2', 'demmatin2', 'demsoir2', 'color2', 'matin3',
      'soir3', 'demmatin3', 'demsoir3', 'color3', 'matin4', 'soir4', 'demmatin4', 'demsoir4',
      'color4', 'matin5', 'soir5', 'demmatin5', 'demsoir5', 'color5')).toBeTruthy();
  });


  it('should create an instance from a JSON Object', () => {
    const agent = new Mois('WATTIEZ Philippe','WATTIEZ','philippe.wattiez@cerema.fr','2021-11-01',
      '01/11/20-05/11/20','C','C','Rien','Rien','light','T','T','Rien','Rien','light','T','T',
      'Rien','Rien','light','T','T','Rien','Rien','light','T','T','Rien','Rien','light');
    const json = JSON.parse('{"agent":"WATTIEZ Philippe","nom":"WATTIEZ",'+
      '"email":"philippe.wattiez@cerema.fr","date":"2021-11-01","semaine":"01\/11\/20-05\/11\/20",'+
      '"matin1":"C","soir1":"C","demmatin1":"Rien","demsoir1":"Rien","color1":"light",'+
      '"matin2":"T","soir2":"T","demmatin2":"Rien","demsoir2":"Rien","color2":"light","matin3":"T",'+
      '"soir3":"T","demmatin3":"Rien","demsoir3":"Rien","color3":"light","matin4":"T","soir4":"T",'+
      '"demmatin4":"Rien","demsoir4":"Rien","color4":"light","matin5":"T","soir5":"T",'+
      '"demmatin5":"Rien","demsoir5":"Rien","color5":"light"}');
    expect(Mois.fromJson(json)).toEqual(agent);
  });

  it('should static function conv convert like previous: T  => Mobilisable sur site', () => {
    expect(Mois.conv('T')).toEqual('Mobilisable sur site');
  });
  it('should static function conv convert like previous: TT => Mobilisable hors site', () => {
    expect(Mois.conv('TT')).toEqual('Mobilisable hors site');
  });
  it('should static function conv convert like previous: C  => Ne pas déranger', () => {
    expect(Mois.conv('C')).toEqual('Ne pas déranger');
  });
  it('should static function conv convert like previous: else => Inconnu', () => {
    expect(Mois.conv('?')).toEqual('Inconnu');
  });
});
