/**
 * Classe représentant un service du Cerema avec les information provenant d'ASPRO
 */
export class Services {
    /** Sigle court du service sans le chemin complet (exemple EPTN) */
    sigle: string;
    /** Désignation longue du service (optionnel) */
    designation: string;
    /** sigle du niveau supérieur du service (exemple SGI)   */
    rattachement: string;
    /** Identifiant de l'Agent responsable de service, du type "b3c8f57b-096c-42e1-a1e4-eae734fb9122"  */
    responsable: string;
    /** Nombre du service, permettant le filtrage en ne prenant que les nb multiple de niveau (1 = toute la DSI) */
    niveau: number;
    /** détermine s'il est visible ou pas en fonction que son service est déplié ou replié */
    cache: boolean = false;
    /** Libellé complet du service , exemple DSI/SGI/EPTN */
    libelle: string;
    /** niveau par défaut */
    defaut: number;
    /** Couleur dans le thème pour visualiser les services et leurs agents */
    color: string;

    /**
     * Constructeur de la classe Services à partir des éléments en base, calcule les autres champs non transmis
     *
     * @param sigle Sigle court du service sans le chemin complet (exemple EPTN)
     * @param designation Désignation longue du service
     * @param rattachement sigle du niveau supérieur du service (exemple SGI)
     * @param responsable Identifiant de l'Agent responsable de service, du type "b3c8f57b-096c-42e1-a1e4-eae734fb9122"
     * @param niveau Nombre du service, permettant le filtrage en ne prenant que les nb multiple de niveau (1 = toute la DSI)
     * @param color Couleur dans le thème pour visualiser les services et leurs agents
     * @param chemin Libellé complet du service , exemple DSI/SGI/EPTN
     */
     constructor(sigle: string,designation: string,rattachement: string,responsable: string,
        niveau: string,color: string,chemin: string) {
        this.sigle = sigle;
        this.designation = designation;
        this.rattachement = rattachement;
        this.responsable = responsable;
        this.niveau = parseInt(niveau,10);
        this.color = color;
        this.libelle = chemin;
    }

    /**
     * Fonction statique de classe permettant de transformer une response HTTP en JSON en une instance de la classe Services
     *
     * @param json Objet json transmis suite à une requete HTTP API REST
     * @returns Un objet de la classe Services alimenté à partir des attributs JSON
     */
    public static fromJson(json): Services {
        return new Services(
            json.sigle,
            json.designation,
            json.rattachement,
            json.responsable,
            json.niveau,
            json.color,
            json.chemin
           );
    }
}
