import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { NavController } from '@ionic/angular';
import { OAuthService, UrlHelperService,OAuthModule, OAuthLogger,DateTimeProvider } from 'angular-oauth2-oidc';
import { AuthenticationService } from 'src/app/services/authentication.service';


import { Tab1Page } from './tab1.page';

describe('Tab1Page', () => {
  let component: Tab1Page;
  let fixture: ComponentFixture<Tab1Page>;
  const agent: Agent = {
    id: '90',
    identifiant: 'a2e8ca99-890c-47a0-aca0-5c8e3d0477b1',
    agent: 'WATTIEZ Philippe',
    site: null,
    telephone: '+33 482917538',
    mobile: '+33 661649314',
    service: 'EPTN',
    fonction: 'Chef de pôle',
    email: 'philippe.wattiez@cerema.fr',
    complet: 'DSI/SGI/EPTN',
    designation: 'Pôle études, projets et transformation numérique',
    trigramme: 'PWA'
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tab1Page],
      providers: [ApiserviceService,AuthenticationService,NavController,AgentService,OAuthService,
         UrlHelperService, DateTimeProvider,OAuthLogger],
      imports: [IonicModule.forRoot(),HttpClientTestingModule,RouterTestingModule,
        ExploreContainerComponentModule,OAuthModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab1Page);
    component = fixture.componentInstance;
    component.api = TestBed.inject(ApiserviceService);
    component.nav = TestBed.inject(NavController);
    component.user  = TestBed.inject(AgentService);
    component.user.setAgent(agent);
    fixture.detectChanges();
  }));

  afterEach(() => {
    component.user.videStorage().then(() => console.log('Locale storage vidé'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
