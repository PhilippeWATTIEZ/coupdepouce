import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { Agents } from '../models/agents';
import { NavController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';

/**
 * Composant de la page de l'onglet 1 : Organigramme
 */
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

/**
 * Classe associée au composant Page du premier onglet
 */
export class Tab1Page implements OnInit {
  /** Variable contenant la chaine de recherche d'un agent dans l'organigramme */
  searchTerm: string ='';
  /** Agent connecté en tant qu'utilisateur */
  agent: Agent;
  /** Liste des agents de l'organigramme contenu dans un tableau d'objet Agent */
  agents: Agents[];
  /** Icone affichée en cours pour déployer ou réduire l'organigramme */
  sens: string = 'caret-down-circle-outline';
  /** Désignation de la date du jour au format long pour affichage en clair */
  aujourdhui: string;


  /**
   * Constructeur de la page avec les services nécessaires à son fonctionnement
   *
   * @param api Service d'accès aux informations en backoffice via API REST
   * @param nav Service de controle de la navigation
   * @param user Service de stockage en locale des informations de connexion
   */
  constructor(public api: ApiserviceService,public nav: NavController,public user: AgentService, private auth: AuthenticationService) {
    this.user.getObservable().subscribe(x => {
      this.agent = x;
      if ( x !== null && x !== undefined && x.email !== '') {
        this.api.getAgents().subscribe(y => {
          this.agents = y;
          this.searchTerm='';
        });
      }
    });
  }

  /**
   * Fonction implémentée avec la classe pour initialiser les éléments de la page dans le cycle de vie
   *
   *
   * - initialisation de la date du jour
   * - vérification que l'utilisateur est connecté, sinon affichage de la page connexion
   * - récupération en base de l'organigramme
   * - vidage de la chaine de filtrage (recherche d'un agent en particulier)
   */
  ngOnInit() {
    this.aujourdhui = new Date(Date.now()).toLocaleDateString('fr-FR');
    this.agent= this.user.getAgent();
    if ( this.agent !== null && this.agent !== undefined && this.agent.email !== '') {
      this.api.getAgents().subscribe(y => {
        this.agents = y;
        this.searchTerm='';
      });
    } else {
      this.connexion();
    }
  }

  /**
   * Fonction de type bascule qui réduit ou déploye l'organigramme en fonction de l'état actuel
   *
   * @returns Aucune information
   */
  change(): void {
    if ( this.sens === 'caret-down-circle-outline') {
      this.agents.forEach(element => {
        element.cache=false;
        element.icone = 'caret-up-outline';
      });
      this.sens = 'caret-up-circle-outline';
    } else
    {
      this.agents.forEach(element => {
        element.nb = element.defaut;
        element.cache = false;
        element.icone = 'caret-down-outline';
        if (element.designation === '' && element.nb > 0) { element.cache = true; }
        if (element.designation !== '' && element.nb < 0) { element.icone = 'caret-up-outline'; }
      });
      this.sens = 'caret-down-circle-outline';
    }
  }

  /**
   * Fonction déclenchée chaque fois que le contenu de la zone de formulaire recherche est modifiée
   *
   * - force les agents à être tous cachés
   * - recherche les agents correspondant au masque de saisi et les rend visibles
   *
   * @returns Aucun information
   */
  searchChanged(): void{
    if ( this.searchTerm.length > 2)
    {
      this.sens = 'caret-down-circle-outline';
      this.change();
      this.agents.forEach(element => {
          if (element.agent.toUpperCase().match(this.searchTerm.toUpperCase()) ||
              element.site.toUpperCase().match(this.searchTerm.toUpperCase())) { element.cache=false; }
          else { element.cache=true; }
      });
    }
  }

  /**
   * Fonction de bascule en visible ou caché un niveau de service spécifique (click sur une icone)
   *
   * @param value Valeur spécifique d'un service, les sous services sont des multiples
   */
  bascule(value: number): void{
    this.agents.forEach(element => {
        if ( element.nb % Math.abs(value) === 0 ) {
          element.nb*=-1;
          if ( value > 0 ) {element.cache=false; element.icone = 'caret-up-outline';}
          else {
            if( element.designation !== '' ) { element.icone ='caret-down-outline'; }
            else { element.cache=true; }
          }
        }
    });
  }

  /**
   * Fonction de navigation qui permet d'aller sur l'onglet Mois d'un agent en particulier
   *
   * @param email Email permettant d'identifier l'agent concerné par l'affichage des information du mois
   */
  mois(email: string): void{
    this.nav.navigateForward([`tabs/tab3/${email}`]);
  }

  /**
   * Fonction permettant d'aller à l'onglet affichage d'une semaine pour un site en particulier
   *
   * @param site Identifiant du site concerné par l'affichage des données semaines
   */
  semaine(site: string): void{
    const date = new Date(Date.now()).toJSON().substring(0, 10);
    this.nav.navigateForward([`tabs/tab2/${date}/1/${site}`]);
  }

  /**
   * Fonction permettant d'afficher la page concenant les données du profil de l'agent sélectionné
   *
   * @param id Identifiant de l'agent doit il faut afficher le profil
   */
  profil(id: string){
    this.nav.navigateForward([`profil/${id}`]);
  }

  /**
   * Fonction permettant d'accèder directement à la page de connexion/déconnexion
   *
   * @returns Aucune information
   */
  connexion(): void{
    this.nav.navigateForward([`connexion`]);
  }

  /**
   * Fonction permettant d'aller à l'onglet affichage d'une semaine pour un service en particulier
   *
   * @param niveau Valeur spécifique d'un service, les sous services sont des multiples
   */
  vasemaine(niveau: number): void{
    const date = new Date(Date.now()).toJSON().substring(0, 10);
    this.nav.navigateForward([`tabs/tab2/${date}/${niveau}/%25`]);
  }

  /**
   * Fonction demandant la reconnexion à Orion
   */
  orion(): void{
    if ( this.auth.isAuthenticated() ) {
      this.auth.logout();
    }
    else {
      this.auth.login();
    }
  }
}
