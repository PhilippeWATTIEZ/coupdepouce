import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { NavController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Tab2Page } from './tab2.page';
import { of } from 'rxjs';
import { Services } from '../models/services';
import { Semaine } from '../models/semaine';
import { OAuthService, UrlHelperService, OAuthLogger,OAuthModule,DateTimeProvider } from 'angular-oauth2-oidc';


describe('Tab2Page', () => {
  let component: Tab2Page;
  let fixture: ComponentFixture<Tab2Page>;
  const agent: Agent = {
    id: '90',
    identifiant: 'a2e8ca99-890c-47a0-aca0-5c8e3d0477b1',
    agent: 'WATTIEZ Philippe',
    site: null,
    telephone: '+33 482917538',
    mobile: '+33 661649314',
    service: 'EPTN',
    fonction: 'Chef de pôle',
    email: 'philippe.wattiez@cerema.fr',
    complet: 'DSI/SGI/EPTN',
    designation: 'Pôle études, projets et transformation numérique',
    trigramme: 'PWA'
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tab2Page],
      providers: [ApiserviceService,AuthenticationService,NavController,AgentService,OAuthService,
         UrlHelperService, OAuthLogger,DateTimeProvider],
      imports: [IonicModule.forRoot(),HttpClientTestingModule,RouterTestingModule,
        ExploreContainerComponentModule,OAuthModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab2Page);
    component = fixture.componentInstance;
    component.api = TestBed.inject(ApiserviceService);
    component.nav = TestBed.inject(NavController);
    component.user  = TestBed.inject(AgentService);
    component.user.setAgent(agent);
    fixture.detectChanges();
  }));

  afterEach(() => {
    component.user.videStorage().then(() => console.log('Locale storage vidé'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get user / sites / services / semaine (stub)', () => {

    // création des objets fictifs
    const services: Services[] = [{sigle:'DSI',designation:'Direction des systèmes d\'information',
      rattachement:'',responsable:'b3c8f57b-096c-42e1-a1e4-eae734fb9122',niveau:1,cache:false,
      libelle:'DSI',defaut:1,color:'primary'},{sigle:'DIR',designation:'Direction',
      rattachement:'DSI',responsable:'b3c8f57b-096c-42e1-a1e4-eae734fb9122',niveau:11,cache:false,
      libelle:'DSI/DIR',defaut:1,color:'secondary'}];
    component.api.getServices = () => of(services);
    const sites: string[] = ['AIX-EN-PROVENCE','BLOIS','BRON','CHAMPS-SUR-MARNE','L\'ISLE D\'ABEAU',
      'LE GRAND-QUEVILLY','LES PONTS-DE-CE','LILLE','LYON','MARGNY-LES-COMPIEGNE','METZ','NANTES',
      'PLOUZANE','SAINT-BRIEUC','SAINT-QUENTIN','SOURDUN','STRASBOURG','TOULOUSE','TRAPPES'];
    component.api.getSites = () => of(sites);
    const semaine: Semaine[] = [{email: 'jean-pierre.troeira@cerema.fr',designation: 'Direction',
      agent: 'TROEIRA Jean-Pierre',niveau: 11,tri: '1',matin1: 'T',soir1: 'T',matin2: 'T',soir2: 'T',
      matin3: 'T',soir3: 'T',matin4: 'T',soir4: 'T',matin5: 'T',soir5: 'T',demmatin1: 'Rien',
      demsoir1: 'Rien',demmatin2: 'Rien',demsoir2: 'Rien',demmatin3: 'Rien',demsoir3: 'Rien',
      demmatin4: 'Rien',demsoir4: 'Rien',demmatin5: 'Rien',demsoir5: 'Rien',color: 'medium',
      nom: 'TROEIRA',m1: 'T',s1: 'T',m2: 'T',s2: 'T',m3: 'T',s3: 'T',m4: 'T',s4: 'T',m5: 'T',s5: 'T'},
      {email: 'patrick.berge@cerema.fr',designation: 'Direction',agent: 'BERGE Patrick',niveau: 11,
      tri: '1',matin1: 'T',soir1: 'T',matin2: 'T',soir2: 'T',matin3: 'T',soir3: 'T',matin4: 'T',
      soir4: 'T',matin5: 'T',soir5: 'T',demmatin1: 'Rien',demsoir1: 'Rien',demmatin2: 'Rien',
      demsoir2: 'Rien',demmatin3: 'Rien',demsoir3: 'Rien',demmatin4: 'Rien',demsoir4: 'Rien',
      demmatin5: 'Rien',demsoir5: 'Rien',color: 'medium',nom: 'BERGE',m1: 'T',s1: 'T',m2: 'T',
      s2: 'T',m3: 'T',s3: 'T',m4: 'T',s4: 'T',m5: 'T',s5: 'T'}];
    component.api.getSemaine = (date,niveau,site) => of(semaine);
    component.user.getAgent = () => agent;

    // initialisation de la page
    component.ngOnInit();

    // vérification de la fourniture des éléments requis
    expect(component.agent).toEqual(agent);
    expect(component.sites).toEqual(sites);
    //expect(component.semaines).toEqual(semaine);
    expect(component.services).toEqual(services);

  });

});
