import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../services/apiservice.service';
import { AgentService } from '../services/agent.service';
import { Agent } from '../models/agent';
import { Services } from '../models/services';
import { NavController } from '@ionic/angular';
import { Semaine } from '../models/semaine';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { apiurl } from 'src/environments/environment';

/**
 * Composant associé à la page du deuxième onglet : semaine
 */
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

/**
 * Classe associé au composant de la page du deuxième onglet: semaine
 */
export class Tab2Page implements OnInit {
  /** Tableau des noms de services pour alimenter la liste déroulante de filtrage */
  services: Services[];
  /** Tableau des informations concernant les position des agents filtrer pour la semaine  */
  semaines: Semaine[];
  /** Tableau des noms de sites pour alimenter la liste déroulante de filtrage */
  sites: string[];
  /** Variable sauvegarde la dernière valeur sélectionnée dans la liste déroulante du formulaire site */
  site: string = '%25';
  /** Date fixant la semaine en cours d'affichage */
  date: string = new Date(Date.now()).toJSON().substring(0, 10);
  /** Variable associée  avec la liste déroulante du formulaire Service */
  selectedService: string = '1';
  /** Variable associée avec la liste déroulante du formulaire site */
  selectedSite: string = '%25';
  /** Variable sauvegarde la dernière valeur sélectionnée dans la liste déroulante du formulaire service */
  niveau: number = 1;
  /** Titre à afficher dans le bandeau */
  titre: string;
  /** Informations concernant l'utilisateur connecté */
  agent: Agent;
  /** Options spécifiques à la liste déroulante des services */
  customPopoverOptions: any = {
    header: 'Services de la DSI',
    subHeader: 'Choix du service',
    cssClass: 'grandelargeur'
  };
  /** Options spécifiques à la liste déroulante des sites */
  customPopoverOptionsSite: any = {
    header: 'Sites de la DSI',
    subHeader: 'Choix du site',
    cssClass: 'grandelargeur'
  };

  /**
   * Constructeur de la classe avec les services nécessaires à son fonctionnement
   *
   * @param api Service d'accès aux informations en backoffice via API REST
   * @param nav Service de controle de la navigation
   * @param user Service de stockage en locale des informations de connexion
   * @param activatedRoute Service de manipulation des informations de requêtes
   */
  constructor(public api: ApiserviceService,public nav: NavController,public user: AgentService,
      public activatedRoute: ActivatedRoute, private auth: AuthenticationService) {
    this.user.getObservable().subscribe(x => {
      this.agent = x;
      if ( x !== null && x !== undefined && x.email !== '') {
        this.api.getSemaine(this.date,this.niveau,this.site).subscribe(y => {
          this.semaines = y;
        });
      }
    });
  }

  /**
   * Fonction implémentée avec la classe pour initialiser les éléments de la page dans le cycle de vie
   *
   *
   * - initialisation de la date du jour: date fourni dans la route sinon la date du jour
   * - initialisation du service sélectionné: service fourni dans la route sinon 1 (toute la DSI)
   * - initialisation du site sélectionné: site fourni dans la route sinon % (toute les sites)
   * - vérification que l'utilisateur est connecté, sinon affichage de la page connexion
   * - calcul du titre à afficher dans le bandeau
   * - alimentation des listes déroulantes services, site et du tableau de résultats
   */
  ngOnInit() {
    if ( this.activatedRoute.snapshot.paramMap.has('date')) { this.date = this.activatedRoute.snapshot.paramMap.get('date'); }
    else { this.date = new Date(Date.now()).toJSON().substring(0, 10); }
    if ( this.activatedRoute.snapshot.paramMap.has('niveau')) {
      this.niveau = parseInt(this.activatedRoute.snapshot.paramMap.get('niveau'),10);
      this.selectedService = this.activatedRoute.snapshot.paramMap.get('niveau');
    }
    if ( this.activatedRoute.snapshot.paramMap.has('site')) {
      this.site = this.activatedRoute.snapshot.paramMap.get('site');
      this.selectedSite = this.site;
    }
    this.titre = this.calcul(this.date);
    this.selectedService = this.niveau.toString();
    this.api.getServices().subscribe(x => {
      this.services = x;
    });
    this.api.getSites().subscribe(x => {
      this.sites = x;
    });
    this.agent=this.user.getAgent();
    if ( this.agent !== null && this.agent !== undefined && this.agent.email !== '') {
      this.api.getSemaine(this.date,this.niveau,this.site).subscribe(y => {
        this.semaines = y;
      });
    }
  }

  /**
   * Fonction appelé lorsque l'on utilise le ion-refresher, elle reactualise les données avec celles en base de données
   *
   * @param event Evénement qui a déclenché l'appel de la fonction sur la page
   */
  doRefresh(event): void{
    this.api.getSemaine(this.date,this.niveau,this.site).subscribe(x => this.semaines = x,
      () => console.log('erreur de rechargement'),
      () => event.target.complete()
    );
  }

  /**
   * Compare 2 objets service de la liste déroulante
   *
   * @param o1 premier élément de comparaison
   * @param o2 deuxième élément de comparaison
   * @returns Vrai si les deux objet sont identiques, Faux sinon
   */
  compareniveau(o1: number,o2: number): boolean{
    return o1 && o2 ? o1 === o2 : o1 === o2;
  }

  /**
   * Compare 2 objets site de la liste déroulante
   *
   * @param o1 premier élément de comparaison
   * @param o2 deuxième élément de comparaison
   * @returns Vrai si les deux objet sont identiques, Faux sinon
   */
  comparesite(o1: string,o2: string){
    return o1 && o2 ? o1 === o2 : o1 === o2;
  }

  /**
   * Calcul à partir d'une date, la semaine en identifiant les bornes du lundi au vendredi
   *
   * @param date Date à transformer
   * @returns Une chaine de caractère du type 'du dd/mm/YYYY au dd/mm/YYYY'
   */
  calcul(date: string){
    const now = new Date(date);
    //const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    const decal = (now.getDay()===0 ? 7 : now.getDay())-1;
    const debut = now.valueOf() - (decal * 24 * 60 * 60 * 1000);
    console.log(new Date(debut).toLocaleDateString('fr-FR'));
    const result= new Date(debut+ (5 * 23 * 60 * 60 * 1000)).toLocaleDateString('fr-FR');
    return 'du '+new Date(debut).toLocaleDateString('fr-FR') + ' au ' + result;
  }

  /**
   * Fonction déclenchée par un changement de la date dans le formulaire
   */
  dateChange(){
    this.titre = this.calcul(this.date);
    this.api.getSemaine(this.date,this.niveau,this.site).subscribe(x => {
      this.semaines = x;
    });
  }

  /**
   * Fonction permettant d'accéder à la page de connexion/déconnexion
   */
  connexion(): void{
    this.nav.navigateForward([`connexion`]);
  }

  /**
   * Fonction permettant de réinitialiser les données à partir de la base de données,
   * en fonction des critères de sélection en cours (service, site, date)
   */
  reinit(): void{
    if ( parseInt(this.selectedService,10) !== this.niveau || this.selectedSite!== this.site ) {
      this.niveau = parseInt(this.selectedService,10);
      this.site = this.selectedSite;
      this.api.getSemaine(this.date,this.niveau,this.site).subscribe(x => {
        this.semaines = x;
      });
    }
  }

  /**
   * Appel de la génération en backoffice du tableau Excel récapitulatif des prochains mois
   */
  recap(){
    window.open(apiurl+'/recap/feuilleService5.php?serv='+this.selectedService,'_blank');
  }

  /**
   * Fonction qui permet d'alterner les couleurs des lignes successives du tableau
   *
   * @param color Radical de la classe CSS de la ligne du tableau
   * @param i N° de ligne du tableau
   * @returns En fonction de la parité de la ligne, retourne le nom de la classe à utiliser
   */
  classe(color: string, i: number): string{
    const index= i%2;
    return color + index;
  }

  /**
   * Fonction demandant la reconnexion à Orion
   */
   orion(): void{
    if ( this.auth.isAuthenticated() ) {
      this.auth.logout();
    }
    else {
      this.auth.login();
    }
  }
}
