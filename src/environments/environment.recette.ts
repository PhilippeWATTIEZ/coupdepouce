import { AuthConfig } from 'angular-oauth2-oidc';
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

/** Variable qui défini les paramètres et informtions propres à l'environnement de production */
export const environment = {
  /** boolean qui permet de savoir si l'on est sur l'environnemet de production */
  production: false,
  /** boolean qui permet de savoir si l'on est sur l'environnemet de recette */
  recette: true
};

/** Variable Objet qui défini les paramétres de oauthService en fonction de l'environnement */
  export const authCodeFlowConfig: AuthConfig = {
    // Url of the Identity Provider
    issuer: 'https://orion-recette.cerema.fr/auth/realms/CeremaApps',

    // URL of the SPA to redirect the user to after login
    redirectUri: 'https://coupdepouce-recette.cerema.fr/connexion',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: 'coupdepouce-recette.cerema.fr',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',
    dummyClientSecret:  'c99aa5b4-c0aa-4939-a6e2-56734700e0b2',

    responseType: 'code',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    scope: 'openid email',

    showDebugInformation: true,
  };

/** Variable qui défini l'URL de base des endpoints de l'API REST en fonction de l'environnement */
export const apiurl: string = '/apimuse/';


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
