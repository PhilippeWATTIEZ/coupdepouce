import { AuthConfig } from 'angular-oauth2-oidc';

/** Variable qui défini les paramètres et informtions propres à l'environnement de production */
export const environment = {
  /** boolean qui permet de savoir si l'on est sur l'environnemet de production */
  production: true,
  /** boolean qui permet de savoir si l'on est sur l'environnemet de recette */
  recette: false
};

/** Variable Objet qui défini les paramétres de oauthService en fonction de l'environnement */
export const authCodeFlowConfig: AuthConfig = {
  // Url of the Identity Provider
  issuer: 'https://orion.cerema.fr/auth/realms/CeremaApps',

  // URL of the SPA to redirect the user to after login
  redirectUri: 'https://coupdepouce.cerema.fr/connexion',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  clientId: 'coupdepouce.cerema.fr',

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',
  dummyClientSecret:  'd35072c1-39bf-4e40-9770-d64aea1c5515',

  responseType: 'code',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  scope: 'openid email',

  showDebugInformation: true,
};


/** Variable qui défini l'URL de base des endpoints de l'API REST en fonction de l'environnement */
export const apiurl: string = '/apimuse/';

