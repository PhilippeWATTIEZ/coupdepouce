# Comment contribuer

Ce projet est avant tout un démonstrateur pour mettre en oeuvre le CI/CD au Cerema, qui a un intérêt pour les agents de la DSI.
Il est destiné à être déployé sur des infrastructures de PROD, lié à la base de données SITT.

Ce projet est développé avec le framework IONIC/Angular, qui génére une Progressive Web Apps, sur un environnement de développement NodeJs, avec une documentation générée par compodoc, avec des tests unitaires automatisés par Karma/Jasmine, et un analyse de la qualité du code par eslint.

Voici quelques ressources importantes :

  * [Ionic](https://ionicframework.com/) présentation du framework,
  * [Karma/Jasmine](https://atomrace.com/tester-son-application-angular-avec-karma-et-jasmine/) initiation aux tests unitaires NodeJs
  * [CompoDoc](https://compodoc.app/) est l'outil de génération de la documentation
  * [esLint](https://angular.io/cli/lint) est l'outil d'analyse de qualité du code source

Le but est de rendre SGI autonome pour valider la solution qui assurera le service offert par la DSI pour l'intégration continue et le déploiement continu.
