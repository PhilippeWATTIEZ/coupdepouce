# Coup de pouce

### Faire la procédure d'installation classique
- attente modèle de dossier existant

### Faire un fichier Dockerfile ou docker-compose.yml pour le déploiement en docker
- modifier le fichier actuelle pour tester en développement

### Faire un déploiement automatisé en complétant le fichier .gitlab-ci.yml
- formation à finaliser et attente de cluster Nutanix / Kubernetes